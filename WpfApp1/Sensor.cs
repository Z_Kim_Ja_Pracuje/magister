﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class Sensor
    {
        protected bool _isWorking;
        protected int _ID;
        protected bool _hasNewData;
        protected int _newDataCount;

        public bool IsWorking {
            get { return _isWorking; }
            protected set { _isWorking = value; }
        }

        public int ID {
            get { return _ID; }
            set {
                if(value >= 0)
                {
                    _ID = value;
                }
            }
        }

        public bool HasNewData {
            get => _hasNewData;
            set => _hasNewData = value;
        }

        public int NewDataCount {
            get => _newDataCount;
            set => _newDataCount = value;
        }

        public V3 ActAngles => Data.Last().ActAngles;
        public double ActTime => Data.Last().ActTime;

        //public bool HasNewData2 {
        //    get {
        //        return Data.Last().
        //    }
        //}

        public Sensor(int id, bool isWorking)
        {
            ID = id;
            IsWorking = isWorking;
            Data = new List<DataRow>(50000);
            HasNewData = false;
            NewDataCount = 0;
        }

        public Sensor() : this(0, true) { }

        // jakaś struktura do przechowywania danych
        // zestaw list? tylko trzeba przygotować metody do liczenia z automatu

        public List<DataRow> Data { get; set; }

        /// <summary>
        /// 'Odbiór' danych z pliku
        /// </summary>
        /// <param name="row"></param>
        /// <param name="time"></param>
        public void ReceiveData(string row, double time) {
            Data.Add(new DataRow(row, time));
        }

        /// <summary>
        ///  Odbiór danych - bezprzewodowo
        /// </summary>
        /// <param name="array"></param>
        /// <param name="time"></param>
        public void ReceiveData(byte[] array, double time, double prevTime) {
            ReceiveData(new DataRow(array, time, prevTime));
        }

        public void ReceiveData(DataRow data) {
            Data.Add(data);
            HasNewData = true;
            NewDataCount++;
        }

        public void ReceiveEmpty() {
            Data.Add(new DataRow());
        }

        public void CalculateQuaternion() {
            DataRow lastRow = Data.Last();
            lastRow.Q = Quat.FromEuler(lastRow.Euler);
        }

        public void WriteToStream(StreamWriter st) {
            st.WriteLine("Time;AccX;AccY;AccZ;GyroX;GyroY;GyroZ;MagX;MagY;MagZ;Roll;Pitch;Yaw;Cos;X;Y;Z");
            foreach(var row in Data) {
                st.WriteLine(row.ToString());
            }
            st.Close();
        }

        public void LoadFromFile(Stream s) {
            // pytanie, czy czyścić
            using(StreamReader sr = new StreamReader(s)) {
                // pierwsza linia to tekst, dopiero kolejne to dane
                sr.ReadLine();
                while (!sr.EndOfStream) {
                    string line = sr.ReadLine();
                    // split and insert
                    Data.Add(new DataRow(line, Data.Any() ? Data.Last().ActTime : 0));
                }
            }
        }

        public DataRow GetValue(int counter = -1) {
            if (Data == null || !Data.Any())
                return null;
            if (counter == -1)
                return Data.Last();
            else
                return Data[counter];
        }
    }
}
