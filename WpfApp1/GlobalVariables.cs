﻿using log4net;
using OxyPlot.Wpf;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using WpfApp1.Model;

namespace WpfApp1
{
    /// <summary>
    /// Typ symulacji - skąd biorą się dane
    /// </summary>
    public enum Type
    {
        Wire = 1, // czujniki kablowe - tego nie będzie (podobno) ale opcję zostawiam
        Wireless = 2, // czujniki bezprzewodowe
        File = 4 // pliki
    }

    public enum DiagData {
        Accelerometer = 1,
        Gyroscope = 2,
        Magnetometer = 4,
        Euler = 8,
        Quaternion = 16,
        CalculatedAngles = 32
    }

    /// <summary>
    /// Dane globalne - typ symulacji, itd.
    /// NIE UŻYWAĆ PRZED WYWOŁANIEM Initialise()!
    /// </summary>
    public static class GlobalVariables {

        // log
        private static readonly ILog _log = LogManager.GetLogger("");

        // Zmienne 
        private static Type _simulationType;
        private static SensorManager _sensorManager;
        private static CalculationManager _calculationManager;
        private static ChartManager _chartManager;
        private static double _startTime;
        private static DateTime _startT;
        private static Plot[] _diagnosticCharts;
        private static int _insertedCount = 0;
        private static double _filterRatio = 0.02;
        private static DiagData _diagData = DiagData.Accelerometer;
        private static Pacjent _patientData;
        private static Data _data;
        private static Data _dataCompare;
        private static object _dataLocker;
        private static object _dataCompareLocker;
        private static int _visibleRange = 10;

        
        private static ZipArchive _zip;
        private static bool _isCompare;


        // czujniki bezprzewodowe
        /// <summary>
        /// Początek ramki
        /// </summary>
        public static byte RS_SOP => 0x1b; // początek ramki

        /// <summary>
        /// Escape - znak specjalny ramki
        /// </summary>
        public static byte RS_DLE => 0x10; // bajt specjalny

        /// <summary>
        /// Koniec ramki
        /// </summary>
        public static byte RS_EOP => 0x0d; // koniec ramki

        /// <summary>
        /// Wartość siły grawitacji (N)
        /// </summary>
        public static double Gravity => 9.81;
        
        /// <summary>
        /// Ilość bajtów danych, które przypadają na jeden odczyt dla jednego czujnika
        /// </summary>
        public static int WirelessDataSize => 29;

        /// <summary>
        /// Dryft żyroskopu (stopnie na sekundę)
        /// </summary>
        public static double GyroDrift => 5;

        /// <summary>
        /// Wartość, jaką musi zwrócić akcelerometr, aby można było uznać, że jest jakiś krok
        /// </summary>
        public static double AccTick => 6;

        /// <summary>
        /// Czas jaki musi upłynąć, aby można było uznać, że jest następny krok
        /// </summary>
        public static double TimeToWait => 0.1;

        /// <summary>
        /// Z ilu iteracji zliczać średnią przy wyznaczaniu początku kroku.
        /// </summary>
        public static int IterationCount => 3;
        

        /// <summary>
        /// Typ sumulacji
        /// </summary>
        public static Type SimulationType {
            get { return _simulationType; }
            private set { _simulationType = value; }
        }

        /// <summary>
        /// Referencja do menadżera czujników - nie używać w inny sposób!
        /// </summary>
        public static SensorManager SensorManager {
            get { return _sensorManager; }
            private set { _sensorManager = value; }
        }

        /// <summary>
        /// Referencja do menadżera obliczeń - nie używać w inny sposób!
        /// </summary>
        public static CalculationManager CalculationManager {
            get { return _calculationManager; }
            private set { _calculationManager = value; }
        }

        /// <summary>
        /// Referencja do menadżera wykresów - nie używać w inny sposób!
        /// </summary>
        public static ChartManager ChartManager {
            get { return _chartManager; }
            private set { _chartManager = value; } 
        }

        /// <summary>
        /// Czas rozpoczęcia badania [s]
        /// </summary>
        public static double StartTime {
            get { return _startTime; }
            set { _startTime = value; }
        }

        /// <summary>
        /// Czas rozpoczęcia badania
        /// </summary>
        public static DateTime StartT {
            get { return _startT; }
            set { _startT = value; }
        }

        /// <summary>
        /// Tablica 3 wykresów na zakładce 'Wykresy diagnostyczne'
        /// </summary>
        public static Plot[] DiagnosticCharts {
            get => _diagnosticCharts;
            set => _diagnosticCharts = value;
        }

        /// <summary>
        /// Counter - ile wierszy danych zostało odebranych
        /// </summary>
        public static int InsertedCount {
            get => _insertedCount;
            set => _insertedCount = value;
        }

        /// <summary>
        /// Współczynnik filtru komplementarnego
        /// </summary>
        public static double FilterRatio {
            get => _filterRatio;
            private set => _filterRatio = value;
        }

        /// <summary>
        /// Jakie dane mają być wyświetlane na wykresach diagnostycznych
        /// </summary>
        public static DiagData DiagData {
            get => _diagData;
            private set => _diagData = value;
        }

        /// <summary>
        /// Tablica czujników
        /// </summary>
        public static Sensor[] SensorList {
            get { return SensorManager?.SensorList; }
        }

        /// <summary>
        /// Dane pacjenta
        /// </summary>
        public static Pacjent PatientData {
            get => _patientData;
            set => _patientData = value;
        }

        /// <summary>
        /// Dane badania - czyli policzone kąty w stawach
        /// </summary>
        public static Data Data {
            get => _data;
            private set => _data = value;
        }

        /// <summary>
        /// Policzone kąty w stawach w ramach opcji porównywania wyników z poprzednimi
        /// </summary>
        public static Data DataCompare {
            get => _dataCompare;
            private set => _dataCompare = value;
        }

        /// <summary>
        /// Locker dla obiektu Data - żeby dało się używać danych w różnych wątkach
        /// </summary>
        public static object DataLocker {
            get => _dataLocker;
            private set => _dataLocker = value;
        }

        /// <summary>
        /// Locker dla obiektu DataCompare - żeby dało się używać w różnych wątkach
        /// </summary>
        public static object DataCompareLocker {
            get => _dataCompareLocker;
            private set => _dataCompareLocker = value;
        }

        /// <summary>
        /// Jaki zakres czasu ma być wyświetlany na wykresach (w sekundach)
        /// </summary>
        public static int VisibleRange {
            get => _visibleRange; 
            private set => _visibleRange = value; 
        }

        /// <summary>
        /// Zip z pomiarami - używany przy wczytywaniu z plików i przy porównywaniu
        /// </summary>
        public static ZipArchive Zip {
            get => _zip;
            set => _zip = value;
        }

        public static MainWindow MainWindow { get; set; }

        /// <summary>
        /// Czy dane wczytywane z plików posłużą do wczytania danych pomiaru na czysto czy do porównania wyników
        /// </summary>
        public static bool IsCompare {
            get => _isCompare;
            set => _isCompare = value;
        }


        /// <summary>
        /// Inicjalizacja zawartości klasy 
        /// </summary>
        /// <param name="simType">Typ - czy z czujników, czy z plików</param>
        /// <param name="diagnostic">Tablica z wykresami diagnostycznymi</param>
        /// <param name="pd">Dane pacjenta</param>
        /// <param name="compareData">Czy porównywanie danych</param>
        public static void Initialise(Type simType, Plot[] diagnostic, Pacjent pd,
            Grid leftLegCharts, Grid rightLegCharts, Grid diagCharts, Grid chosenCharts = null)
        {
            // trzeba jeszcze przesłać wszystkie wykresy

            SimulationType = simType;
            StartT = DateTime.MinValue;
            StartTime = 0.0;

            DiagnosticCharts = diagnostic; 
            InsertedCount = 0;

            PatientData = pd; 

            DataLocker = new object();
            DataCompareLocker = new object();

            // tymczasowo na sztywno ustawione 4 czujniki - trzeba to uwzględnić dalej
            Sensor[] s = new Sensor[4];
            for (int i = 0; i < s.Length; i++) {
                s[i] = new Sensor(i, true);
            }
            Data = new Data();
            SensorManager = new SensorManager(s);
            CalculationManager = new CalculationManager(s, Data);
            ChartManager = new ChartManager(s, Data, leftLegCharts, rightLegCharts, 
                diagCharts, chosenCharts);

            Zip = null;
            // SensorManager = new SensorManager();
            SetCompareSensorList();
            
            switch (simType) {
                case Type.Wireless: {
                    

                    //if (compareData) {
                    //    DataCompare = new Data();
                    //    // trzeba ustawiać w CM i ChM - albo i nie, wyjdzie w praniu
                    //}
                    
                    break;
                }
                case Type.File: {
                    // 1 z dwóch - albo wczytywanie danych pacjenta z plików
                    // albo z danych
                    // bo dane w plikach trzeba zunifikować

                    // jest sens tu cokolwiek robić? wszelkie operacje powinny być wykonywane w Logic, nie tutaj

                    break;
                }
            }
        }

        public static void SetCompareSensorList() {
            if(IsCompare && SensorManager != null) {
                SensorManager.SetCompareSensorList();
                DataCompare = new Data();
            }
        }
        
        
        public static void ChangeDiagData(string type) {
            switch (type) {
                case "Akcelerometry":
                    DiagData = DiagData.Accelerometer;
                    break;
                case "Żyroskopy":
                    DiagData = DiagData.Gyroscope;
                    break;
                case "Magnetometry":
                    DiagData = DiagData.Magnetometer;
                    break;
            }


        }

        #region Extensions

        public static int ReadAll(this SerialPort serialPort, byte[] buffer) {
            // trzeba zmienic - jezeli bufor za krotki - albo lap wyjatek i zgub dane
            // albo rozszerz bufor
            return serialPort.Read(buffer, 0, serialPort.BytesToRead);
        }

        /// <summary>
        /// Metoda zwraca średnią z listy zaokrągloną do 1 miejsca po przecinku
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">Source is null</exception> 
        /// <exception cref="System.InvalidOperationException">Source contains no elements</exception>
        
        public static string AverageStr(this List<double> list, int start, int count) {
            return list.GetRange(start, count).Average().ToString("F1");
        }

        /// <summary>
        /// Zwraca minimum z listy zaokrąglone do 1 miejsca po przecinku 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">Source is null</exception> 
        /// <exception cref="System.InvalidOperationException">Source contains no elements</exception>
        public static string MinStr(this List<double> list, int start, int count) {
            if (start + count >= list.Count) return "";
            return list.GetRange(start, count).Min().ToString("F1");
        }

        /// <summary>
        /// Zwraca maksimum z listy zaokrąglone do 1 miejsca po przecinku
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">Source is null</exception> 
        /// <exception cref="System.InvalidOperationException">Source contains no elements</exception>
        public static string MaxStr(this List<double> list, int start, int count) {
            return list.GetRange(start, count).Max().ToString("F1");
        }

        public static List<T> GetIndexRange<T>(this List<T> list, int start, int end) {
            return list.GetRange(start, end - start + 1);
        }
        #endregion
    }
}
