﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WpfApp1 {
    public class SensorSimulate {
        private List<byte> _bytesToRead = new List<byte>();
        private DateTime _startTime = DateTime.MinValue;
        private DateTime _lastReadByte = DateTime.MinValue;
        private int _interval = 100;
        private int _sensorCount = 8;
        private Random _random = new Random();
        // private int _current = 0;

        public List<byte> BytesToRead { get => _bytesToRead; set => _bytesToRead = value; }
        public DateTime StartTime { get => _startTime; set => _startTime = value; }
        public int Interval { get => _interval; set => _interval = value; }
        public DateTime LastGenerateBytes { get => _lastReadByte; set => _lastReadByte = value; }
        public int SensorCount { get => _sensorCount; set => _sensorCount = value; }

        private List<byte> byteTime = new List<byte>();
        private List<byte> newByteTime = new List<byte>();
        private List<byte> tmp = new List<byte>();
        private readonly object locker = new object();
        

        public byte ReadByte() {
            if(StartTime == DateTime.MinValue) {
                StartTime = DateTime.Now;
            }

            if(BytesToRead.Count == 0 && (DateTime.Now - LastGenerateBytes).TotalMilliseconds < Interval) {
                Thread.Sleep(Interval - (int)(DateTime.Now - LastGenerateBytes).TotalMilliseconds);
            }

            //Queue<byte> q = new Queue<byte>();
            //q.A

            if((DateTime.Now - LastGenerateBytes).TotalMilliseconds >= Interval) {
                var time = (UInt16)(DateTime.Now - StartTime).TotalMilliseconds;
                byteTime = (BitConverter.IsLittleEndian ? BitConverter.GetBytes(time).ToList() : BitConverter.GetBytes(time).Reverse().ToList());
                newByteTime.Clear();
                foreach(byte b in byteTime) {
                    if (b == GlobalVariables.RS_DLE || b == GlobalVariables.RS_EOP || b == GlobalVariables.RS_SOP) {
                        newByteTime.Add(GlobalVariables.RS_DLE);
                        newByteTime.Add((byte)(255 - b));
                    }
                    else {
                        newByteTime.Add(b);
                    }
                }

                int i = 0;
                for (i = 0; i < SensorCount; i++) {
                    lock (locker) {
                        try {

                            tmp.Clear();
                            // początek ramki
                            tmp.Add(GlobalVariables.RS_SOP);
                            tmp.Add((byte)'R');
                            tmp.Add((byte)(i + 1));
                            tmp.Add((byte)'R');

                            tmp.AddRange(newByteTime);
                            for (int j = 0; j < 11; j++) {
                                byte[] newOne = null;
                                do {
                                    var rand = (short)_random.Next(0, short.MaxValue);
                                    newOne = (BitConverter.IsLittleEndian ? BitConverter.GetBytes(rand)
                                        : BitConverter.GetBytes(rand).Reverse().ToArray());
                                }
                                while (newOne.Any(x => x == GlobalVariables.RS_DLE || x == GlobalVariables.RS_EOP || x == GlobalVariables.RS_SOP));

                                tmp.AddRange(newOne);
                            }
                        }
                        catch (Exception ex) {

                        }

                        tmp.Add(GlobalVariables.RS_EOP);
                        BytesToRead.AddRange(tmp);
                    }
                }
                LastGenerateBytes = DateTime.Now;
                Console.WriteLine($"Generate bytes: {LastGenerateBytes}; Time: {time}; ByteTime: {byteTime.Select(x=>x.ToString()).Aggregate((o, p) => $"{o}, {p}")}; " +
                    $"Generated sensors: {i}");

            }

            if(BytesToRead.Count > 0) {
                var toReturn = BytesToRead[0];
                BytesToRead.RemoveAt(0);
                return toReturn;
            }
            //else if
            // to nigdy nie powinno się wykonać - ale może
            Console.WriteLine("NIEPRAWIDŁOWE WYKONANIE - NIGDY NIE POWINNO DOJŚĆ DO KOŃCA");
            return 0;
        }

        private void GenerateData() {

        }

    }
}
