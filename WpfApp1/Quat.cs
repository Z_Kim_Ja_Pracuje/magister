﻿using System;

namespace WpfApp1 {
    public class Quat {
        #region fields
        private double angleInRad;

        public double cos { get; set; }
        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }

        public double angle {
            get{
                return angleInRad * 180 / Math.PI;
            }
        }

        public double angleRad{
            get{
                return angleInRad;
            }
            private set{
                angleInRad = value;
            }
        }

        public V3 axis { get; set; }

        public double length{
            get{
                return Math.Sqrt(cos * cos + x * x + y * y + z * z);
            }
        }

        public Quat conjugate{
            get{
                return new Quat(cos, -x, -y, -z);
            }
        }

        public Quat inverse{
            get{
                return conjugate / (length * length);
            }
        }
        #endregion

        #region ctors
        public Quat(string cos, string x, string y, string z) {

            try {
                this.cos = double.Parse(cos);
                this.x = double.Parse(x);
                this.y = double.Parse(y);
                this.z = double.Parse(z);
            }
            catch(Exception e) {
               // LogFile.writeToLog(e);
            }

            setAngle();
        }

        public Quat(double cos, double x, double y, double z) {
            this.cos = cos;
            this.x = x;
            this.y = y;
            this.z = z;
            setAngle();
        }

        public Quat(double cos, V3 ax) : this(cos, ax.X, ax.Y, ax.Z) { }

        public Quat() {
            cos = 0.0;
            x = 0.0;
            y = 0.0;
            z = 0.0;
        }

        public Quat(Quat q) {
            cos = q.cos;
            x = q.x;
            y = q.y;
            z = q.z;
        }

        private Quat(double angle) {
            angleRad = (Math.PI / 180.0) * angle;
            cos = x = y = z = 0.0;
        }
        #endregion

        #region methods

        private void setAngle() {
            double sin2 = 1.0 - this.cos * this.cos;
            if(sin2 <= 0.0) {
                axis = new V3(0, 0, 0);
                angleInRad = 0.0;
            }
            else {
                angleInRad = Math.Acos(this.cos);
                double sin = 1.0 / Math.Sqrt(sin2);
                axis = new V3(this.x * sin, this.y * sin, this.z * sin);
                
            }
        }

        private double safeAcos(double x) {
            if(x <= -1.0) return Math.PI * 2.0;
            else if(x >= 1.0) return 0;
            else {
                return Math.Acos(x) * 2.0;
            }
        }

        

        public void normalize() {
            double l = length;
            if(l == 0.0) return;
            cos /= l;
            x /= l;
            y /= l;
            z /= l;
            setAngle();
        }

        public V3 toEuler() {
            V3 e = new V3();
            e.X = Math.Atan2(2 * (cos * x + y * z), 1 - 2 * (x * x + y * y));
            e.Y = Math.Asin(2 * (cos * y + z * x));
            e.Z = Math.Atan2(2 * (cos * z + x * y), 1 - 2 * (y * y + z * z));
            e *= (180.0 / Math.PI);
            return e;
        }

        public override string ToString() {
            string t = string.Format("cos = {0}, x = {1}, y = {2}, z = {3}", cos, x, y, z);
            return t;
        }

        public string toStr() {
            string s = string.Format("{0};{1};{2};{3}", Math.Round(cos, 3), Math.Round(x, 3), Math.Round(y, 3), Math.Round(z, 3));
            return s;
        }

        /// <summary>
        /// Przeliczenie kątów eulera na kwaternion
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static Quat FromEuler(V3 e) {
            if (e == null)
                return new Quat();

            e = e * CalculationManager.ToRad/2;

            double cr = Math.Cos(e.X);
            double sr = Math.Sin(e.X);
            double cp = Math.Cos(e.Y);
            double sp = Math.Sin(e.Y);
            double cy = Math.Cos(e.Z);
            double sy = Math.Sin(e.Z);

            Quat q = new Quat();
            //q.w() = cy * cr * cp + sy * sr * sp;
            //q.x() = cy * sr * cp - sy * cr * sp;
            //q.y() = cy * cr * sp + sy * sr * cp;
            //q.z() = sy * cr * cp - cy * sr * sp;
            q.cos = cy * cr * cp + sy * sr * sp;
            q.x = cy * sr * cp - sy * cr * sp;
            q.y = cy * cr * sp + sy * sr * cp;
            q.z = sy * cr * cp - cy * sr * sp;

            return q;
        }

        /// <summary>
        /// Wyznaczenie siły grawitacji z kwaternionu 
        /// </summary>
        /// <returns></returns>
        public V3 GravityForce() {
            throw new NotImplementedException();
        }

        #endregion

        #region operators
        public static Quat operator*(Quat a, Quat b) {
            Quat ret = new Quat();
            ret.x = a.cos * b.x + a.x * b.cos + a.y * b.z - a.z * b.y;

            ret.y = a.cos * b.y + a.y * b.cos + a.z * b.y - a.x * b.z;

            ret.z = a.cos * b.z + a.z * b.cos + a.x * b.y - a.y * b.x;

            ret.cos = a.cos * b.cos - a.x * b.x - a.y * b.y - a.z * b.z;

            ret.setAngle();
            return ret;
        }

        public static Quat operator-(Quat a) {
            return new Quat(-a.cos, -a.x, -a.y, -a.z);
        }

        public static Quat operator/(Quat q, double l) {
            return new Quat(q.cos / l, q.x / l, q.y / l, q.z / l);
        }
        #endregion


        
    }
}
