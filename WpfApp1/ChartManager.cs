﻿using OxyPlot.Wpf;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Threading;

namespace WpfApp1 {
    public class ChartManager {
        


        //
        private Sensor[] _sensorList;
        private Data _data;
        private DateTime _lastScale;
        private DateTime _lastRefresh;
        private double _lastScaleSeconds;
        private double _lastRefreshSeconds;

        private List<Plot> _diagnosticCharts;
        private List<Plot> _leftLegCharts;
        private List<Plot> _rightLegCharts;
        private List<Plot> _chosenCharts;
        private object _chosenChartsLocker = new object();

        private volatile int _diagSensorID;


        private int RefreshChartCounter { get; set; } = 0;
        
        public Sensor[] SensorList {
            get { return _sensorList; }
            set { _sensorList = value; }
        }

        public Data Data {
            get { return _data; }
            set { _data = value; }
        }

        public DateTime LastScale {
            get { return _lastScale; }
            set { _lastScale = value; }
        }

        public DateTime LastRefresh {
            get { return _lastRefresh; }
            set { _lastRefresh = value; }
        }

        public double LastRefreshSeconds {
            get {
                return (LastRefresh - GlobalVariables.StartT).TotalSeconds;
            }
        }

        public double LastScaleSeconds {
            get {
                return (LastScale - GlobalVariables.StartT).TotalSeconds;
            }
        }

        public double ActTime {
            get {
                return (DateTime.Now - GlobalVariables.StartT).TotalSeconds;
            }
        }

        public List<Plot> DiagnosticCharts {
            get => _diagnosticCharts;
            private set => _diagnosticCharts = value;
        }

        public List<Plot> AllCharts {
            get => new List<Plot>(LeftLegCharts).Union(RightLegCharts).Union(ChosenCharts ?? new List<Plot>()).
                Union(ChosenCharts ?? new List<Plot>()).ToList();
        }

        public List<Plot> LeftLegCharts {
            get => _leftLegCharts;
            private set => _leftLegCharts = value;
        }

        public List<Plot> RightLegCharts {
            get => _rightLegCharts;
            private set => _rightLegCharts = value;
        }
        
        public int DiagSensorID {
            get => _diagSensorID;
            set => _diagSensorID = value;
        }

        public List<Plot> ChosenCharts {
            get => _chosenCharts;
            private set => _chosenCharts = value;
        }

        public object ChosenChartsLocker {
            get => _chosenChartsLocker;
            private set => _chosenChartsLocker = value;
        }


        public ChartManager(Sensor[] sensors, Data _data, Grid leftLeg, Grid rightLeg, Grid diagCharts, Grid chosenCharts) {
            SensorList = sensors;
            Data = _data;
            _lastScale = DateTime.MinValue;
            _lastRefresh = DateTime.MinValue;

            LeftLegCharts = MainWindow.GetControls<Plot, Grid>(leftLeg);
            RightLegCharts = MainWindow.GetControls<Plot, Grid>(rightLeg);
            DiagnosticCharts = MainWindow.GetControls<Plot, Grid>(diagCharts);

            lock (ChosenChartsLocker) {
                ChosenCharts = chosenCharts == null ? null : MainWindow.GetControls<Plot, Grid>(chosenCharts);
            }

            RefreshChartCounter = 0;
            
        }

        /// <summary>
        /// Metoda uzupełnia właściwość AllCharts na podstawie przekazanych gridów z wykresami
        /// </summary>
        /// <param name="grids"></param>
        public void SetAllCharts(params Grid[] grids) {
            if(grids != null) {
                foreach (var grid in grids) {
                    AllCharts.AddRange(grid.Children.Cast<System.Windows.UIElement>().Where(x => x is Plot).Select(x => x as Plot));
                }
            }
        }

        private void ChartManager_DiagTypeChanged(object sender, EventArgs e) {
            DiagData dd = GlobalVariables.DiagData;
            int tmp = (int)(dd);
            tmp = (int)Math.Log(tmp, 2);
            
            // TestValue(ref tmp, dd);

            for(int i = 0; i < DiagnosticCharts.Count; i++) {
                int c = DiagnosticCharts[i].ActualModel.Series.Count;
                for (int j = 0; j < c; j++) {
                    DiagnosticCharts[i].ActualModel.Series[j].IsVisible = false;
                }
                switch (dd) {
                    // można je ogarnąć na wspólnej logice
                    case DiagData.Accelerometer:
                    case DiagData.Gyroscope:
                    case DiagData.Magnetometer:
                    case DiagData.Euler:
                    case DiagData.CalculatedAngles:
                        for (int j = 0; j < 3; j++)
                            DiagnosticCharts[i].ActualModel.Series[3*tmp + j].IsVisible = true;
                        break;

                    case DiagData.Quaternion:
                        for (int j = 0; j < 4; j++)
                            DiagnosticCharts[i].ActualModel.Series[3 * tmp + j].IsVisible = true;
                        break;
                }
            }
        }

        /// <summary>
        /// Nie wiem po co ta metoda, zostawiam na razie
        /// </summary>
        /// <param name="action"></param>
        public void InvokeChartRefresh(Action action) {
            App.Current.Dispatcher.Invoke(action, 
                System.Windows.Threading.DispatcherPriority.Normal);
        }

        /// <summary>
        /// Przerobić - wklepywać gdzieś z boku
        /// </summary>
        public void InsertDataToDiagnosticCharts() {
            var watch = Stopwatch.StartNew();

            var sensor = SensorList[DiagSensorID];
            var lastRec = sensor.Data.Last();

            OxyPlot.Series.LineSeries[] ls = DiagnosticCharts[0].ActualModel.Series.ToList().ConvertAll<OxyPlot.Series.LineSeries>(x => (OxyPlot.Series.LineSeries)(x)).ToArray();

            ls[0].Points.Add(new DataPoint(lastRec.ActTime, lastRec.Acc.X));
            ls[1].Points.Add(new DataPoint(lastRec.ActTime, lastRec.Acc.Y));
            ls[2].Points.Add(new DataPoint(lastRec.ActTime, lastRec.Acc.Z));

            ls = DiagnosticCharts[1].ActualModel.Series.ToList().ConvertAll<OxyPlot.Series.LineSeries>(x => (OxyPlot.Series.LineSeries)(x)).ToArray();
            ls[0].Points.Add(new DataPoint(lastRec.ActTime, lastRec.Gyro.X));
            ls[1].Points.Add(new DataPoint(lastRec.ActTime, lastRec.Gyro.Y));
            ls[2].Points.Add(new DataPoint(lastRec.ActTime, lastRec.Gyro.Z));

            ls = DiagnosticCharts[2].ActualModel.Series.ToList().ConvertAll<OxyPlot.Series.LineSeries>(x => (OxyPlot.Series.LineSeries)(x)).ToArray();
            ls[0].Points.Add(new DataPoint(lastRec.ActTime, lastRec.Mag.X));
            ls[1].Points.Add(new DataPoint(lastRec.ActTime, lastRec.Mag.Y));
            ls[2].Points.Add(new DataPoint(lastRec.ActTime, lastRec.Mag.Z));

            ls = DiagnosticCharts[3].ActualModel.Series.ToList().ConvertAll<OxyPlot.Series.LineSeries>(x => (OxyPlot.Series.LineSeries)(x)).ToArray();
            ls[0].Points.Add(new DataPoint(lastRec.ActTime, lastRec.Euler.X));
            ls[1].Points.Add(new DataPoint(lastRec.ActTime, lastRec.Euler.Y));
            ls[2].Points.Add(new DataPoint(lastRec.ActTime, lastRec.Euler.Z));

            ls = DiagnosticCharts[4].ActualModel.Series.ToList().ConvertAll<OxyPlot.Series.LineSeries>(x => (OxyPlot.Series.LineSeries)(x)).ToArray();
            ls[0].Points.Add(new DataPoint(lastRec.ActTime, lastRec.ActAngles.X));
            ls[1].Points.Add(new DataPoint(lastRec.ActTime, lastRec.ActAngles.Y));
            ls[2].Points.Add(new DataPoint(lastRec.ActTime, lastRec.ActAngles.Z));

            Console.WriteLine($"InsertDataToDiagnosticCharts, ActTime: {lastRec.ActTime}");

            // przetestować ale chyba pójdzie do osobnego wątku
            GlobalVariables.MainWindow.Dispatcher.Invoke(() => {
                RefreshCharts(DiagnosticCharts);
            }, DispatcherPriority.Normal);

        }

        /// <summary>
        /// Chyba do wywalenia
        /// </summary>
        public void RefreshDiagnosticCharts() {
            for(int i = 0; i < DiagnosticCharts.Count; i++) {
                DiagnosticCharts[i].InvalidatePlot(true);
                DiagnosticCharts[i].ActualModel.InvalidatePlot(true);
                DiagnosticCharts[i].ActualModel.PlotView.InvalidatePlot();
                
            }
        }

        /// <summary>
        /// Chyba do wywalenia
        /// </summary>
        public void RescaleDiagnosticCharts() {
            
            for(int i = 0; i < DiagnosticCharts.Count; i++) {
                var bAxis = DiagnosticCharts[i].Axes.First(x => x.Position == OxyPlot.Axes.AxisPosition.Bottom);
                int diff = (bAxis.Maximum - ActTime >= 1.0) ? 5 : 6;
                bAxis.Maximum += diff;
                bAxis.Minimum += diff;
                bAxis.AbsoluteMaximum += diff;
                //if (i == 3)
                //    Console.WriteLine(bAxis.Minimum);
            }
            RefreshDiagnosticCharts();
        }

        /// <summary>
        /// Wstawienie danych i odświeżenie wszystkich wykresów
        /// Wywoływać w Logic, wstawiać do wykresów
        /// </summary>
        public void InsertData() {
            var charts = LeftLegCharts.Union(RightLegCharts);

            lock (GlobalVariables.DataLocker) {

                GlobalVariables.MainWindow.Dispatcher.Invoke(() => {
                    var timeVector = GlobalVariables.Data.TimeStamps;
                    foreach (var chart in charts) {

                        var lineSeries = (OxyPlot.Series.LineSeries)chart.ActualModel.Series[0];
                        var list = GlobalVariables.Data.GetType().GetRuntimeProperty((chart.Name.Contains("Pelvic") ? chart.Name.Replace("Left", "").Replace("Right", "") : chart.Name))
                            .GetValue(GlobalVariables.Data) as List<double>;
                        if (list != null && list.Any() && list.Count == GlobalVariables.InsertedCount) {
                            lineSeries.Points.Add(new DataPoint(timeVector.Last(), list.Last()));
                        }
                    }
                }, DispatcherPriority.Normal);
            }

            // tymczasowo wyłączam
            InsertDataToDiagnosticCharts();

            // na wypadek, gdyby ostro muliło

            // przetestować - jak się będzie jebać to do osobnego wątku
            // albo async

            GlobalVariables.MainWindow.Dispatcher.Invoke(() => {
                if (++RefreshChartCounter >= 1) {
                    RefreshCharts(charts);
                    RefreshChartCounter = 0;
                }
            }, DispatcherPriority.Normal);
        }

        public void RefreshCharts(IEnumerable<Plot> Charts) {
            var charts = Charts.ToList();
            if (!((OxyPlot.Series.LineSeries)charts[0].ActualModel.Series.First()).Points.Any())
                return;
            if (charts[0].ActualModel.Axes.First(x=>x.Position == OxyPlot.Axes.AxisPosition.Bottom).AbsoluteMaximum <=
                ((OxyPlot.Series.LineSeries)charts[0].ActualModel.Series.First()).Points.Last().X) {
                // rescale + refresh
                // RescaleAllCharts(((OxyPlot.Series.LineSeries)charts[0].ActualModel.Series.First()).Points.Last().X - GlobalVariables.VisibleRange/2);
                RescaleChosenCharts(charts, ((OxyPlot.Series.LineSeries)charts[0].ActualModel.Series.First()).Points.Last().X);
            }
            else {
                // tylko refresh
                RefreshChosenCharts(charts);
            }
        }

        public void RefreshAllCharts() {
            // wszystkie - czyli nie ma sensu robić podziałów
            var charts = LeftLegCharts.Union(RightLegCharts).Union(DiagnosticCharts);
            if(ChosenCharts != null & ChosenCharts.Any()) {
                charts = charts.Union(ChosenCharts);
            }

            RefreshChosenCharts(charts);
            RescaleChosenCharts(charts, 1);
        }

        private void RescaleAllCharts(double value) {
            var charts = LeftLegCharts.Union(RightLegCharts);

            RescaleChosenCharts(charts, value);
        }

        /// <summary>
        /// Przeskalowanie + odświeżenie podanych wykresów 
        /// </summary>
        /// <param name="charts">Wykresy do przeskalowania</param>
        /// <param name="value">Do jaiego momentu w czasie</param>
        private void RescaleChosenCharts(IEnumerable<Plot> charts, double value) {
            foreach(var chart in charts) {
                var bAxis = chart.Axes.First(x => x.Position == OxyPlot.Axes.AxisPosition.Bottom);
                bAxis.Minimum = value - 1;
                bAxis.AbsoluteMinimum = bAxis.Minimum;
                bAxis.Maximum = bAxis.Minimum + GlobalVariables.VisibleRange;
                bAxis.AbsoluteMaximum = bAxis.Maximum;
            }

            RefreshChosenCharts(charts);
        }

        private void RefreshChosenCharts(IEnumerable<Plot> charts) {
            foreach(var chart in charts) {
                chart.InvalidatePlot(true);
                chart.ActualModel.InvalidatePlot(true);
                chart.ActualModel.PlotView.InvalidatePlot();
            }
        }

        /// <summary>
        /// 'Ręczne' przeskalowanie wykresów - za pomocą suwaków
        /// </summary>
        /// <param name="value"></param>
        /// <param name="sliderName"></param>
        public void RescaleCharts(double value, string sliderName) {
            // przeskalowanie - spróbujemy podejścia, gdzie przesuwamy wykresy wybrane na podstawie nazwy slidera
            string rootName = sliderName.Replace("Slider", "");

            if (sliderName.ToLower().EndsWith("czas")) {
                // przewijanie po czasie
                List<Plot> charts;
                if (sliderName.ToLower().Contains("lewa")) {
                    // wykresy lewej nogi
                    charts = LeftLegCharts;
                }
                else if (sliderName.ToLower().Contains("prawa")) {
                    // wykresy prawej nogi
                    charts = RightLegCharts;
                }
                else {
                    charts = new List<Plot>();
                }

                // to trzeba ujednolicić z innymi opcjami przeskalowywania
                RescaleChosenCharts(charts, value);
            }
            else {
                // przewijanie po krokach - trzeba się odwołać do listy przechowującej kroki - tego jeszcze nie ma
                // i nie będzie
            }
        }

        // tutaj bedzie potrzebna referencja do klasy glownej
        // ale to będzie potrzebne dopiero jak będzie więcej czujników
        // i będzie można przechwytywać chód
        public void PrepareCharts(WpfApp1.MainWindow main) {
            // foreach(var tab in main.TabControl.items)

            main.Dispatcher.Invoke(() => {
                // 'zwykłe wykresy'
                // sprawdzić, czy z porównaniem? Chyba nie, porównanie na końcu. Chociaż, jaby było na początku... - trzeba wystawić zmienną w GlobalVariables

                foreach (var ch in LeftLegCharts.Union(RightLegCharts)) {
                    // ch.ActualModel.Series.Clear();
                    ch.ActualModel.Series.Add(new OxyPlot.Series.LineSeries() {
                        StrokeThickness = 1,
                        Color = OxyColors.Black,
                        Smooth = false,
                        LineJoin = LineJoin.Round,
                        MarkerType = MarkerType.None
                    });
                    
                }

                var colors = new OxyColor[] { OxyColors.Green, OxyColors.Black, OxyColors.Blue };
                RefreshChosenCharts(LeftLegCharts.Union(RightLegCharts));

                // wykresy diagnostyczne
                foreach(var ch in DiagnosticCharts) {
                    ch.ActualModel.Series.Clear();
                    foreach(var col in colors) {
                        ch.ActualModel.Series.Add(new OxyPlot.Series.LineSeries() {
                            StrokeThickness = 1,
                            Color = col,
                            Smooth = false,
                            LineJoin = LineJoin.Round,
                            MarkerType = MarkerType.None
                        });
                    }

                    ch.ActualModel.Axes.Clear();
                    ch.ActualModel.Axes.Add(new OxyPlot.Axes.LinearAxis() {
                        Position = OxyPlot.Axes.AxisPosition.Bottom,
                        AbsoluteMaximum = 10,
                        AbsoluteMinimum = 0,
                        Maximum = 10,
                    });

                    ch.ActualModel.Axes.Add(new OxyPlot.Axes.LinearAxis() {
                        Position = OxyPlot.Axes.AxisPosition.Left,
                        Minimum = 0,
                    });

                }
                RefreshDiagnosticCharts();
            }, DispatcherPriority.Normal);

        }

        public void InsertDataFromFiles() {
            var target = (GlobalVariables.IsCompare ? GlobalVariables.DataCompare : GlobalVariables.Data);

            GlobalVariables.MainWindow.Dispatcher.Invoke(() => {
                foreach (var chart in LeftLegCharts.Union(RightLegCharts)) {

                    var pointList = target.GetType().GetRuntimeProperty((chart.Name.Contains("Pelvic") ? chart.Name.Replace("Left", "").Replace("Right", "") : chart.Name)).GetValue(target) as List<double>;
                    
                    if (pointList == null || !pointList.Any())
                        continue;

                    var points = pointList.Select((x, i) => new DataPoint(target.TimeStamps[i], x));

                    //var poin = Data.LeftFeetAngle.Zip(target.TimeStamps, (x, y) => new DataPoint(y, x));
                    
                    // przepisać kod z INsertData z uwzglednieniem, ze moze byc wczytanie a moze byc porownanie
                    // trzeba jakoś pododawać lineSeries

                    //if (GlobalVariables.IsCompare) {
                    //    chart.ActualModel.Series.Add(new OxyPlot.Series.LineSeries() {
                    //        Color = OxyColors.Red,
                    //        StrokeThickness = 1,
                    //        Smooth = false,
                    //        LineJoin = LineJoin.Round,
                    //        MarkerType = MarkerType.None
                    //    });

                        
                    //}

                    var seriesToAdd = chart.ActualModel.Series.LastOrDefault() as OxyPlot.Series.LineSeries;
                    if(seriesToAdd != null) {
                        seriesToAdd.Points.AddRange(points);
                    }
                    chart.InvalidatePlot(true);
                    chart.ActualModel.InvalidatePlot(false);
                }
            }, DispatcherPriority.Normal);

        }
        
    }
}
