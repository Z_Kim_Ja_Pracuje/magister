﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1 {
    public class Data {

        private static readonly ILog _log = LogManager.GetLogger("");

        /// <summary>
        /// Czasy kolejnych odczytów
        /// </summary>
        private List<double> _timeStamps = new List<double>(50000);
        public List<double> TimeStamps => _timeStamps; 

        // dane obliczone - kąty w kolanach, biodrach, kroki etc;

        // lewa noga
        private List<double> _leftFeetAngle = new List<double>(50000);
        private List<double> _leftFeetRotation = new List<double>(50000);
        private List<double> _leftFeetAdduction = new List<double>(50000);

        private List<double> _leftKneeAngle = new List<double>(50000);
        private List<double> _leftKneeRotation = new List<double>(50000);
        private List<double> _leftKneeAdduction = new List<double>(50000);

        private List<double> _leftHipAngle = new List<double>(50000);
        private List<double> _leftHipRotation = new List<double>(50000);
        private List<double> _leftHipAdduction = new List<double>(50000);

        // prawa noga
        private List<double> _rightFeetAngle = new List<double>(50000);
        private List<double> _rightFeetRotation = new List<double>(50000);
        private List<double> _rightFeetAdduction = new List<double>(50000);

        private List<double> _rightKneeAngle = new List<double>(50000);
        private List<double> _rightKneeRotation = new List<double>(50000);
        private List<double> _rightKneeAdduction = new List<double>(50000);

        private List<double> _rightHipAngle = new List<double>(50000);
        private List<double> _rightHipRotation = new List<double>(50000);
        private List<double> _rightHipAdduction = new List<double>(50000);

        // miednica

        private List<double> _pelvicAngle = new List<double>(50000);
        private List<double> _pelvicRotation = new List<double>(50000);
        private List<double> _pelvicAdduction = new List<double>(50000);

        // kroki

        private List<SingleStep> _leftLegSteps = new List<SingleStep>(200);
        private List<SingleStep> _rightLegSteps = new List<SingleStep>(200);
        

        // lewa noga
        public List<double> LeftFeetAngle { get => _leftFeetAngle; set => _leftFeetAngle = value; }
        public List<double> LeftFeetRotation { get => _leftFeetRotation; set => _leftFeetRotation = value; }
        public List<double> LeftFeetAdduction { get => _leftFeetAdduction; set => _leftFeetAdduction = value; }

        public List<double> LeftKneeAngle { get => _leftKneeAngle; set => _leftKneeAngle = value; }
        public List<double> LeftKneeRotation { get => _leftKneeRotation; set => _leftKneeRotation = value; }
        public List<double> LeftKneeAdduction { get => _leftKneeAdduction; set => _leftKneeAdduction = value; }

        public List<double> LeftHipAngle { get => _leftHipAngle; set => _leftHipAngle = value; }
        public List<double> LeftHipRotation { get => _leftHipRotation; set => _leftHipRotation = value; }
        public List<double> LeftHipAdduction { get => _leftHipAdduction; set => _leftHipAdduction = value; }

        // prawa noga
        public List<double> RightFeetAngle { get => _rightFeetAngle; set => _rightFeetAngle = value; }
        public List<double> RightFeetRotation { get => _rightFeetRotation; set => _rightFeetRotation = value; }
        public List<double> RightFeetAdduction { get => _rightFeetAdduction; set => _rightFeetAdduction = value; }

        public List<double> RightKneeAngle { get => _rightKneeAngle; set => _rightKneeAngle = value; }
        public List<double> RightKneeRotation { get => _rightKneeRotation; set => _rightKneeRotation = value; }
        public List<double> RightKneeAdduction { get => _rightKneeAdduction; set => _rightKneeAdduction = value; }

        public List<double> RightHipAngle { get => _rightHipAngle; set => _rightHipAngle = value; }
        public List<double> RightHipRotation { get => _rightHipRotation; set => _rightHipRotation = value; }
        public List<double> RightHipAdduction { get => _rightHipAdduction; set => _rightHipAdduction = value; }


        // miednica

        /// <summary>
        /// Pochylenie miednicy przód/tył
        /// </summary>
        public List<double> PelvicAngle { get => _pelvicAngle; set => _pelvicAngle = value; }

        /// <summary>
        /// Rotacja miednicy
        /// </summary>
        public List<double> PelvicRotation { get => _pelvicRotation; set => _pelvicRotation = value; }

        /// <summary>
        /// Pochylenie miednicy lewo/prawo
        /// </summary>
        public List<double> PelvicAdduction { get => _pelvicAdduction; set => _pelvicAdduction = value; }

        // kroki
        public List<SingleStep> LeftLegSteps { get => _leftLegSteps; set => _leftLegSteps = value; }
        public List<SingleStep> RightLegSteps { get => _rightLegSteps; set => _rightLegSteps = value; }

        public Data() {
            // inicjalizacja wszystkich list
        }


        /// <summary>
        /// Wyczyszczenie danych
        /// </summary>
        public void ClearData() {
            _timeStamps.Clear();
            _leftFeetAdduction.Clear();
            _leftFeetAngle.Clear();
            _leftFeetRotation.Clear();

            _leftHipAdduction.Clear();
            _leftHipAngle.Clear();
            _leftHipRotation.Clear();

            _leftKneeAdduction.Clear();
            _leftKneeAngle.Clear();
            _leftKneeRotation.Clear();

            _leftLegSteps.Clear();

            _pelvicAdduction.Clear();
            _pelvicAngle.Clear();
            _pelvicRotation.Clear();

            _rightFeetAdduction.Clear();
            _rightFeetAngle.Clear();
            _rightFeetRotation.Clear();

            _rightHipAdduction.Clear();
            _rightHipAngle.Clear();
            _rightHipRotation.Clear();

            _rightKneeAdduction.Clear();
            _rightKneeAngle.Clear();
            _rightKneeRotation.Clear();

            _rightLegSteps.Clear();
        }

        /// <summary>
        /// Załadowanie listy stempli czasowych na podstawie pliku z danymi z czujnika
        /// </summary>
        /// <param name="s"></param>
        public void LoadTimeVector(Stream s) {
            using (StreamReader sr = new StreamReader(s)) {
                sr.ReadLine();
                while (!sr.EndOfStream) {
                    string tmp = sr.ReadLine().Split(';')[0];
                    TimeStamps.Add(double.Parse(tmp));
                    
                }
            }
        }

        public List<double> GetListByName(string name) {
            var list = this.GetType().GetProperties().Where(x => x.Name.ToLower() == ((name.ToLower().Contains("pelvic")) ? 
                        name.ToLower().Replace("left", "").Replace("right", "") : name)).FirstOrDefault();

            if(list != null) {
                var value = list.GetValue(this) as List<double>;
                return value;
            }
            return null;
        }
    }
}
