﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model {
    public interface IRepository<T> where T:DataBaseObject {

        bool Add(T entity);
        bool Update(T entity);
        T FindById(int id);
        IList<T> FindAll();

        /// <summary>
        /// Trwałe usunięcie z bazy danych
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Remove(T entity);

        /// <summary>
        /// Usunięcie symboliczne - ustawienie DataUsuniecia na obecną
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Delete(T entity);

    }
}
