﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WpfApp1.Model
{
    /// <summary>
    /// Dane pacjenta
    /// </summary>
    public class Pacjent : DataBaseObject
    {
        /// <summary>
        /// Imię
        /// </summary>
        [XmlElement("Imie")]
        public string Imie { get; set; }

        /// <summary>
        /// Nazwisko
        /// </summary>
        [XmlElement("Nazwisko")]
        public string Nazwisko { get; set; }

        /// <summary>
        /// Wiek
        /// </summary>
        [XmlElement("Wiek")]
        public int Wiek { get; set; }

        /// <summary>
        /// Wzrost
        /// </summary>
        [XmlElement("Wzrost")]
        public int Wzrost { get; set; }

        /// <summary>
        /// Długość lewej strzałki
        /// </summary>
        [XmlElement("LewaStrzalka")]
        public double LewaStrzalka { get; set; }

        /// <summary>
        /// Długość lewego uda
        /// </summary>
        [XmlElement("LeweUdo")]
        public double LeweUdo { get; set; }

        /// <summary>
        /// Szerokość miednicy
        /// </summary>
        [XmlElement("Miednica")]
        public double Miednica { get; set; }

        /// <summary>
        /// Długość prawej strzałki
        /// </summary>
        [XmlElement("PrawaStrzalka")]
        public double PrawaStrzalka { get; set; }

        /// <summary>
        /// Długość prawego uda
        /// </summary>
        [XmlElement("PraweUdo")]
        public double PraweUdo { get; set; }

        /// <summary>
        /// Długość lewego przedramienia
        /// </summary>
        [XmlElement("LewePrzedramie")]
        public double LewePrzedramie { get; set; }

        /// <summary>
        /// Długość lewego ramienia
        /// </summary>
        [XmlElement("LeweRamie")]
        public double LeweRamie { get; set; }

        /// <summary>
        /// Długość prawego przedramienia
        /// </summary>
        [XmlElement("PrawePrzedramie")]
        public double PrawePrzedramie { get; set; }

        /// <summary>
        /// Długość prawego ramienia
        /// </summary>
        [XmlElement("PraweRamie")]
        public double PraweRamie { get; set; }

        /// <summary>
        /// Szerokość w barkach
        /// </summary>
        [XmlElement("SzerokocBarki")]
        public double Klatka { get; set; }
        
    }
}
