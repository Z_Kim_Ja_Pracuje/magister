﻿using LiteDB;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model {
    public abstract class Repository<T> : Repository, IRepository<T> where T:DataBaseObject {
        
        public Repository(string collection) {
            _collectionName = collection;
        }
        
        protected LiteCollection<T> Collection => database?.GetCollection<T>(_collectionName);

        public bool Add(T entity) {
            try {
                entity.SignUtworzenie();
                Collection.Upsert(entity);
                _log.Info($"Inserted: {entity}");
                return true;
            }
            catch(Exception ex) {
                _log.Error(ex);
                return false;
            }
        }

        public bool Delete(T entity) {
            try {
                entity.SignUsuniecie();
                return true;
            }
            catch(Exception ex) {
                _log.Error(ex);
                return false;
            }
        }

        public IList<T> FindAll() {
            try {
                return Collection.FindAll().ToList();
            }
            catch(Exception ex) {
                _log.Error(ex);
                return null;
            }
        }

        public T FindById(int id) {
            try {
                return Collection.FindOne(x => x.Id == id);
            }
            catch(Exception ex) {
                _log.Error(ex);
                return null;
            }
        }

        public bool Remove(T entity) {
            try {
                Collection.Delete(x => x.Id == entity.Id);
                return true;
            }
            catch(Exception ex) {
                _log.Error(ex);
                return false;
            }
        }

        public bool Update(T entity) {
            try {
                Collection.Upsert(entity);
                return true;
            }
            catch(Exception ex) {
                _log.Error(ex);
                return false;
            }
        }
    }

    public abstract class Repository {
        protected static readonly ILog _log = LogManager.GetLogger("");

        protected string _collectionName;

        protected static LiteDB.LiteDatabase database = new LiteDB.LiteDatabase("../../MyDB.db");
    }
}
