﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model {
    public abstract class DataBaseObject {
        public int Id { get; set; }

        public DateTime DataUtworzenia { get; set; }
        public DateTime DataModyfikacji { get; set; }

        public DateTime? DataUsuniecia { get; set; } = null;

        public void SignUtworzenie() {
            DataUtworzenia = DateTime.Now;
            SignModyfikacja();
        }

        public void SignModyfikacja() {
            DataModyfikacji = DateTime.Now;
        }

        public void SignUsuniecie() {
            SignModyfikacja();
            DataUsuniecia = DateTime.Now;
        }

        public bool IsNewEntity => Id <= 0;

    }
}
