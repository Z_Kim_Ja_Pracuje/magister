﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model {
    public static class DbManager {

        private static PacjentRepository _pacjentRepository;
        private static BadanieRepository _badanieRepository;
        

        static DbManager() {
            _pacjentRepository = new PacjentRepository();
            _badanieRepository = new BadanieRepository();
        }

        public static PacjentRepository PacjentRepository {
            get {
                if (_pacjentRepository == null)
                    _pacjentRepository = new PacjentRepository();
                return _pacjentRepository;
            }
        }

        public static BadanieRepository BadanieRepository {
            get {
                if(_badanieRepository == null) {
                    _badanieRepository = new BadanieRepository();
                }
                return _badanieRepository;
            }
        }
    }
}
