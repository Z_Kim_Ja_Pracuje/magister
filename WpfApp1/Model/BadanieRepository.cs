﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model {
    public class BadanieRepository : Repository<Badanie> {
        public BadanieRepository() : base("Badanie") { }

        public List<Badanie> FindByPacjentId(int pacId) {
            try {
                return Collection.Find(x => x.PacjentId == pacId).ToList();
            }
            catch(Exception ex) {
                _log.Error(ex);
                return new List<Badanie>();
            }
        }
    }
}
