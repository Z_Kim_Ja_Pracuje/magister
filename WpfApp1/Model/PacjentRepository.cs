﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model {
    public class PacjentRepository : Repository<Pacjent> {
        
        public PacjentRepository() : base("Pacjent") {
        }
    }
}
