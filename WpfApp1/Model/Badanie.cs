﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model {
    public class Badanie : DataBaseObject {
        public int PacjentId { get; set; }
        public string Plik { get; set; }
        public string SciezkaDoPliku { get; set; }
        public string Notatka { get; set; }
    }
}
