﻿using System;

namespace WpfApp1 {
    public class V3 {
        #region fields
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        #endregion

        #region ctors

        /// <summary>
        /// Domyślny konstruktor
        /// </summary>
        public V3() {
            X = 0.0;
            Y = 0.0;
            Z = 0.0;
        }

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <param name="z">Z</param>
        public V3(double x, double y, double z) {
            X = Math.Round(x, 3);
            Y = Math.Round(y, 3);
            Z = Math.Round(z, 3);
        }

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <param name="z">Z</param>
        public V3(string x, string y, string z) {
            try {
                X = Math.Round(double.Parse(x), 3);
                Y = Math.Round(double.Parse(y), 3);
                Z = Math.Round(double.Parse(z), 3);
            }
            catch(Exception e) {
                //LogFile.writeToLog(e);
            }
        }

        /// <summary>
        /// Konstruktor kopiujący
        /// </summary>
        /// <param name="v"></param>
        public V3(V3 v) :this(v.X, v.Y, v.Z) { }
        #endregion
        #region operators
        public static V3 operator +(V3 v1, V3 v2) =>
            new V3(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);

        public static V3 operator -(V3 v1, V3 v2) =>
            new V3(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);

        public static V3 operator -(V3 v) =>
            new V3(-v.X, -v.Y, -v.Z);

        public static V3 operator *(V3 v, double val) =>
            new V3(v.X * val, v.Y * val, v.Z * val);

        /// <summary>
        /// Iloczyn skalarny
        /// </summary>
        /// <param name="v"></param>
        /// <param name="w"></param>
        /// <returns></returns>
        public static double operator *(V3 v, V3 w) {
            return v.X * w.X + v.Y * w.Y + v.Z * w.Z;
        }

        public static V3 operator/(V3 a, double b) {
            return new V3(a.X / b, a.Y / b, a.Z / b);
        }
        public static V3 operator*(double val, V3 v)=>
            v*val;
        

        #endregion
        #region methods

        public void Normalize() {
            double l = this.Length;
            if(l == 0.0) return;
            this.X /= l;
            this.Y /= l;
            this.Z /= l;
        }

        /// <summary>
        /// Translacja.
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public V3 Translate(V3 a) {
            return new V3(X + a.X, Y + a.Y, Z + a.Z);
        }

        /// <summary>
        /// Iloczyn wektorowy 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static V3 Cross(V3 a, V3 b) {
            return new V3(a.Y * b.Z - a.Z * b.Y, a.Z * b.X - a.X * b.Z, a.X * b.Y - a.Y * b.X);
        }
            
        /// <summary>
        /// Długość wektora.
        /// </summary>
        public double Length
        {
            get { return Math.Sqrt(X * X + Y * Y + Z * Z); }
        }
        /// <summary>
        /// Rotacja o podany wektor. Nie modyfikuje bazowego wektora.
        /// </summary>
        /// <param name="v1">Wektor rotacji</param>
        /// <returns>Zwraca wektor po rotacji.</returns>
        public V3 Rotate(V3 v1) {
            V3 v = (v1 * Math.PI) * (1.0 / 180.0);
            double sina = Math.Sin(v.X);
            double cosa = Math.Cos(v.X);

            double sinb = Math.Sin(v.Y);
            double cosb = Math.Cos(v.Y);

            double sinc = Math.Sin(v.Z);
            double cosc = Math.Cos(v.Z);

            double x = (cosc * cosb) * this.X + (cosc * sinb * sina - cosa * sinc) * this.Y + (sinc * sina + cosa * cosc * sinb) * this.Z;
            double y = (sinc * cosb) * this.X + (cosa * cosc + sinc * sinb * sina) * this.Y + (sinc * sinb * cosa - sina * cosc) * this.Z;
            double z = -sinb * this.X + sina * cosb * this.Y + cosa * cosb * this.Z;

            return new V3(x, y, z);
        }

        public V3 Rotate(double x, double y, double z) {
            return Rotate(new V3(x, y, z));
        }

        public V3 Copy() {
            return new V3(X, Y, Z);
        }

        public V3 Abs() {
            return new V3(Math.Abs(X), Math.Abs(Y), Math.Abs(Z));
        }

        public override string ToString() {
            return X + " " + Y + " " + Z;
        }

        public string ToStr() {
            return string.Format("{0};{1};{2}", X, Y, Z);
        }

        public override bool Equals(object obj)
        {
            var v = obj as V3;
            return v != null &&
                   X == v.X &&
                   Y == v.Y &&
                   Z == v.Z;
        }

        public override int GetHashCode()
        {
            var hashCode = -307843816;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            hashCode = hashCode * -1521134295 + Z.GetHashCode();
            return hashCode;
        }
        #endregion
    }
}
