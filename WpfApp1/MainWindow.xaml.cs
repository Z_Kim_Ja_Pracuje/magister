﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OxyPlot;
using System.IO.Ports;
using System.Timers;
using OxyPlot.Wpf;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.IO.Compression;
using log4net;
using MahApps.Metro.Controls;
using Newtonsoft.Json;
using WpfApp1.Model;
using System.Windows.Controls.Primitives;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private static readonly ILog _log = LogManager.GetLogger("");

        public List<DataPoint> Test { get; set; }

        // to coś nie chce działać bez uruchomionej aplikacji, tj. podczas debugowania działa,
        // w designerze zwraca jakieś gunwo
        public  int ScreenHeight =>  (int)SystemParameters.VirtualScreenHeight - 40;
        public  int ScreenWidth => (int)SystemParameters.VirtualScreenWidth;

        /// <summary>
        /// Czy przeprowadzać analizę danych w czasie rzeczywistym - czyli czy kolorować pola na zakładkach z danymi.
        /// </summary>
        public bool RealTimeAnalysis { get; protected set; }
        
        /// <summary>
        /// Czy przeliczać dane na zakładkach z danymi w czasie rzeczywistym - bez tej opcji będzie działać
        /// szybciej - po prostu przeliczy wszystko po zatrzymaniu odbierania.
        /// </summary>
        public bool RealTimeDataCalc { get; protected set; }

        /// <summary>
        /// Czy wykonywać porównanie otrzymanych wyników z zapisanymi wynikami.
        /// </summary>
        public bool ResultCompare { get; protected set; }

        /// <summary>
        /// Ścieżka do pliku, z którego będą pobrane wyniki do porównania.
        /// </summary>
        public string ResultCompareFilePath { get; protected set; }

        /// <summary>
        /// Ścieżka do pliku z danymi pacjenta
        /// </summary>
        public string PatientDataFilePath { get; protected set; }
        
        /// <summary>
        /// Przechowuje aktualną ilość wykresów na zakładce 'Wybrane wykresy'
        /// </summary>
        private int WybraneWykresyCounter { get; set; } = 0;

        /// <summary>
        /// Czy jest aktualnie wykonywane badanie
        /// </summary>
        private bool IsSimulation { get; set; } = false;

        /// <summary>
        /// Wątek do liczenia danych w czasie rzeczywistym
        /// </summary>
        private Timer CalculateDataTimer { get; set; }
        
        /// <summary>
        /// Dane pacjenta - zmienna tutaj i w GlobalVariables po to, aby można było po zakończeniu pomiaru wyczyścić
        /// wybór pacjenta na liście.
        /// </summary>
        private Pacjent PatientData { get; set; }

        /// <summary>
        /// Czy pomiar został zatrzymany.
        /// </summary>
        private bool Stopped { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private Logic Logic { get; set; }

        public MainWindow()
        {
            InitializeComponent();
           //  Selection = "Akcelerometry";
            Grid1.Children.Cast<System.Windows.UIElement>().Where(x => x is OxyPlot.Wpf.Plot).Select(x => x as Plot).
                Select(x => new { Value = x.Name, Name = x.Title });

            List<object> comboList = new List<object>();
            comboList.AddRange(GridLewaNogaWykresy.Children.Cast<System.Windows.UIElement>().Where(x => x is Plot).Select(x => x as Plot).
                Select(x => new { Value = x.Name, Name = x.Title }));

            comboList.AddRange(GridLewaNogaWykresy.Children.Cast<System.Windows.UIElement>().Where(x => x is Plot).Select(x => x as Plot).
                Select(x => new { Value = x.Name, Name = x.Title }));


            ComboBoxDodajWykres.ItemsSource = comboList;

            CalculateDataTimer = new Timer();
            CalculateDataTimer.Interval = 1000;
            CalculateDataTimer.Elapsed += CalculateDataTimer_Elapsed;
            
            // test
            Test = new List<DataPoint>();
            var r = new Random();
            for (int i = 0; i < 1000; i++)
                Test.Add(new DataPoint(i*0.01, r.Next(10)));
            this.DataContext = this;

            ListViewPacjenci.ItemsSource = DbManager.PacjentRepository.FindAll().ToList();
            // jeszcze lista badań -> ale to po wyborze pacjenta

        }

        private void CalculateDataTimer_Elapsed(object sender, ElapsedEventArgs e) {
            CalculateData();
        }

        #region Listenery

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            GlobalVariables.ChangeDiagData((sender as ComboBox).SelectedItem as string);

            // Selection = (sender as ComboBox).SelectedItem as string;
        }

        private void CheckBoxPodgladDanych_Checked(object sender, RoutedEventArgs e) {
            RealTimeDataCalc = IsChecked(sender);
        }

        private void CheckBoxAnalizaDanych_Checked(object sender, RoutedEventArgs e) {
            RealTimeAnalysis = IsChecked(sender);
        }
        
        /// <summary>
        /// Sprawdzenie, czy dane liczbowe (zmiennoprzecinkowe są poprawne)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumberTextBoxValidation(object sender, TextCompositionEventArgs e) {
            e.Handled = !double.TryParse(e.Text, out double res);
        }

        /// <summary>
        /// Sprawdzenie, czy dane liczbowe (całkowite) są poprawne
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IntTextBoxValidation(object sender, TextCompositionEventArgs e) {
            e.Handled = !int.TryParse(e.Text, out int res);
        }
        
        /// <summary>
        /// Metoda sprawdza, czy formularz z danymi pacjenta jest poprawnie wypełniony.
        /// Uzupełni również label z komunikatem, co jest nie tak.
        /// </summary>
        /// <returns>True jeśli ok, w przeciwnym wypadku false.</returns>
        private bool ValidatePatientDataForm() {
            Grid grid = (FindName("GridDanePacjentaForm") as Grid);

            var textBoxes = GetControls<TextBox, Grid>(grid);
            
            // sprawdzenie, czy są dane
            string emptyFields = "";
            foreach(var tb in textBoxes) {
                if (string.IsNullOrEmpty(tb.Text)) {
                    emptyFields += $"{(FindName(tb.Name.Replace("TextBox", "Label")) as Label).Content.ToString()}, ";
                }
            }

            if (!string.IsNullOrEmpty(emptyFields)) {
                emptyFields = emptyFields.TrimEnd(new char[] { ' ', ',' });
                TextBlockDanePacjentaInfo.Text = $"Uzupełnij pola: {emptyFields}";
                TextBlockDanePacjentaInfo.Foreground = Brushes.Red;
                return false;
            }

            // walidacja, czy dane są poprawne - nie wiem, czy będzie potrzebna

            TextBlockDanePacjentaInfo.Text = "";
            TextBlockDanePacjentaInfo.Foreground = Brushes.Black;
            return true;
        }

        /// <summary>
        /// Przesuwanie wykresów sliderem - po czasie
        /// TRZEBA DOROBIĆ - UNIWERSALNE PRZESUWANIE, NA RAZIE DZIAŁA TYLKO NA I ZAKŁADCE
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SliderCzas_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e) {
            Slider s = sender as Slider;
            // Console.WriteLine(e.VerticalChange);

            string sliderNameRoot = s.Name.Replace("Slider", "").Replace("Czas", "");

            // bez zabaw - grid tymczasowo stricte przypisany - wszystko będzie działać po załataniu logiki

            var charts = GridLewaNogaWykresy.Children.Cast<System.Windows.UIElement>().Where(x => x is Plot).Select(x => x as Plot).ToList();
            
            foreach(var chart in charts) {
                var bAxis = chart.Axes.First(x => x.Position == OxyPlot.Axes.AxisPosition.Bottom);
                bAxis.Minimum = s.Value;
                bAxis.Maximum = s.Value + 10;
                bAxis.AbsoluteMinimum = s.Value;
                bAxis.AbsoluteMaximum = bAxis.Maximum;

                chart.InvalidatePlot(true);
                chart.ActualModel.InvalidatePlot(true);
                chart.ActualModel.PlotView.InvalidatePlot();
            }
            // GlobalVariables.ChartManager.RescaleCharts()
        }

        /// <summary>
        /// Przesuwanie wykresów sliderem - uniwersalny, cała logika w ChartManager
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SliderWykresyValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
            Slider s = sender as Slider;
            string sliderNameRoot = s.Name.Replace("Slider", "").Replace("Czas", "");
            
            GuiOperation(() => {
                GlobalVariables.ChartManager.RescaleCharts(s.Value, s.Name);
            });
        }

        /// <summary>
        /// Dodawanie wykresu na zakładce 'Wybrane wykresy'.
        /// TRZEBA JESZCZE DOROBIĆ GŁĘBOKĄ KOPIĘ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DodajWykres_Click(object sender, RoutedEventArgs e) {
            try {
                if (this.WybraneWykresyCounter > 11) return; // dopuszczam max 12 wykresów

                var selected = ComboBoxDodajWykres.SelectedValue.ToString();

                var chart = FindName(selected) as Plot;
                if (chart != null) {
                    GuiOperation(() => {
                        var plot = new Plot() { DataContext = chart.DataContext };
                        plot.Margin = chart.Margin;
                        plot.Title = chart.Title;
                        plot.TitleFontSize = chart.TitleFontSize;
                        plot.TitleFontWeight = chart.TitleFontWeight;

                        var bottomAxis = chart.Axes.First(x => x.Position == OxyPlot.Axes.AxisPosition.Bottom);

                        plot.Axes.Add(new LinearAxis() {
                            Position = OxyPlot.Axes.AxisPosition.Bottom,
                            AbsoluteMinimum = bottomAxis.AbsoluteMinimum,
                            AbsoluteMaximum = bottomAxis.AbsoluteMaximum,
                            Maximum = bottomAxis.Maximum,
                            Minimum = bottomAxis.Minimum,
                            IsPanEnabled = false,
                            IsZoomEnabled = false
                        });

                        var leftAxis = chart.Axes.First(x => x.Position == OxyPlot.Axes.AxisPosition.Left);

                        plot.Axes.Add(new LinearAxis() {
                            Position = OxyPlot.Axes.AxisPosition.Left,
                            Minimum = leftAxis.Minimum,
                            Maximum = leftAxis.Maximum,
                            IsPanEnabled = false,
                            IsZoomEnabled = false
                        });
                        
                        LineSeries lineSeries = chart.Series.Any() ? chart.Series[0] as LineSeries : null;
                        var newSeries = new LineSeries() {
                            Width = 2,
                            Color = Colors.Black,
                            Visibility = Visibility.Visible,
                            ItemsSource = lineSeries?.ItemsSource ?? new List<DataPoint>(),
                            LineJoin = LineJoin.Miter,
                            Smooth = false,

                        };
                        plot.Series.Add(newSeries);
                        //newSeries.Points.AddRange()
                        plot.ActualModel.InvalidatePlot(true);
                        
                        int row = (WybraneWykresyCounter + 1) / GridWybraneWykresy.ColumnDefinitions.Count;
                        int column = (WybraneWykresyCounter) % GridWybraneWykresy.ColumnDefinitions.Count;

                        Grid.SetRow(plot, row);
                        Grid.SetColumn(plot, column);
                        GridWybraneWykresy.Children.Add(plot);
                        GridWybraneWykresy.InvalidateVisual();
                        WybraneWykresyCounter++;
                    });
                }
            }
            catch(Exception ex) {
                _log.Error(ex);
            }
        }

        /// <summary>
        /// Jeszcze przeskalowanie danych
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ButtonRozpocznijBadanie_Click(object sender, RoutedEventArgs e) {

            // jeżeli dane są wczytywane z pliku to trzeba sprawdzić, czy dane w pliku są ok
            // więc albo przerabiamy metodę Validate albo wczytujemy dane do formularza i yolo
            // skłaniam się ku opcji nr 2

            // trzeba wszystkie wspólne rzeczy - czyszczenie danych, blokowanie kontrolek etc. jakoś wydzielić do osobnych metod
            
            e.Handled = true;
            if (e.Handled) {


                // I NIBY JAK MA WCZYTYWAC DANE Z PLIKU????
                // TRZEBA PRZEROBIC - JEZELI IsCompare TO WCZYTUJ Z ZIPA!!!!!
                Type t = (RadioWczytajZPliku.IsChecked ?? false) ? Type.File : Type.Wireless;

                
                //GlobalVariables.Initialise(t, new Plot[] { DiagnosticChart1, DiagnosticChart2, DiagnosticChart3, DiagnosticChart4 },
                //    (IsChecked(CheckBoxSymulacja) ? null : PatientData), GridLewaNogaWykresy,GridPrawaNogaWykresy, GridWykresyDiagnostyczne, GridWybraneWykresy);
                GlobalVariables.Initialise(t, new Plot[] { DiagnosticChart1, DiagnosticChart2, DiagnosticChart3, DiagnosticChart4 },
                    PatientData, GridLewaNogaWykresy, GridPrawaNogaWykresy, GridWykresyDiagnostyczne, GridWybraneWykresy);

                GlobalVariables.MainWindow = this;
                GlobalVariables.ChartManager.PrepareCharts(this);


                if((RadioNowyPomiar.IsChecked ?? false) || (RadioNowyZPorownaniem.IsChecked ?? false)) {

                    GlobalVariables.IsCompare = false;

                    if (RadioNowyZPorownaniem.IsChecked ?? false) {
                        // ustawienie zmiennych odnośnie porównania
                        // + wczytanie danych porównania - to trochę potrwa, trzeba to uwzględnić
                        GlobalVariables.IsCompare = true;
                        GlobalVariables.SetCompareSensorList();
                        await LoadFromFiles();
                    }

                    IsSimulation = true;

                    Logic = Logic.Instance;
                    Logic.BeginSimulation(IsChecked(CheckBoxSymulacja));

                    GuiOperation(() => {
                        SliderLewaNogaCzas.IsEnabled = false;
                        SliderPrawaNogaCzas.IsEnabled = false;
                        RangeSliderLewaNoga.IsEnabled = false;
                        RangeSliderPrawaNoga.IsEnabled = false;

                        // nie można dodawać wykresów w trakcie, tylko przed lub po
                        ButtonDodajWykres.IsEnabled = false;
                        ButtonUsunWykres.IsEnabled = false;

                        // powinno załatwić sprawę
                        TabControlMain.SelectedItem = TabItemLewaWykresy;
                        TabItemMenu.IsEnabled = false;
                    });

                    // odświeżanie wykresów - pasuje w wątku, zobaczymy jak to będzie działać

                    if (this.RealTimeDataCalc) {
                        CalculateDataTimer.Enabled = true;
                        CalculateDataTimer.AutoReset = true;
                    }
                    Stopped = false;
                }
                else if(RadioWczytajZPliku.IsChecked ?? false) {

                    await LoadFromFiles();
                    // wczytywanie z plików
                    //var badanie = (ListViewPomiary.SelectedItem as Badanie);
                    //if(badanie != null) {
                    //    try {
                    //        ZipArchive zip = ZipFile.OpenRead(badanie.SciezkaDoPliku);
                    //        GlobalVariables.Zip = zip;
                    //        Logic = Logic.Instance;

                    //        // wyświetlenie komunikatu, żeby czekać na zakończenie

                    //        //Popup popup = new Popup();
                    //        //TextBlock txt = new TextBlock();
                    //        //txt.Text = "Wczytywanie danych, proszę czekać...";
                    //        //txt.Margin = new Thickness(4);

                    //        //ProgressBar pb = new ProgressBar();
                    //        //pb.Minimum = 0;
                    //        //pb.Maximum = 100;
                    //        //pb.IsIndeterminate = true;
                    //        //pb.Margin = new Thickness(4);
                    //        //pb.MinWidth = 200;
                    //        //pb.Background = Brushes.White;

                    //        //StackPanel st = new StackPanel();
                    //        //st.Orientation = Orientation.Vertical;
                    //        //st.Background = Brushes.White;
                    //        //st.Children.Add(txt);
                    //        //st.Children.Add(pb);

                    //        //popup.Child = st;
                    //        //popup.MinWidth = 300;
                    //        //popup.Placement = PlacementMode.Center;
                    //        //popup.PlacementTarget = this;
                    //        //popup.IsOpen = true;

                    //        Logic.BeginSimulation();

                    //        if (this.RealTimeDataCalc) {
                    //            await Task.Run(() => Dispatcher.Invoke(() => this.CalculateData()));
                    //        }

                    //        // popup.IsOpen = false;

                    //    }
                    //    catch(Exception ex) {
                    //        _log.Error(ex);
                    //    }
                    //}
                }
            }
        }

        private async Task LoadFromFiles(bool calculateData = true) {
            var badanie = (ListViewPomiary.SelectedItem as Badanie);
            if (badanie != null) {
                try {
                    ZipArchive zip = ZipFile.OpenRead(badanie.SciezkaDoPliku);
                    GlobalVariables.Zip = zip;
                    Logic = Logic.Instance;

                    Logic.BeginSimulation();

                    if (this.RealTimeDataCalc && calculateData) {
                        await Task.Run(() => Dispatcher.Invoke(() => this.CalculateData()));
                    }
                }
                catch(Exception ex) {
                    _log.Error(ex);
                }
            }

            GlobalVariables.IsCompare = false;
        }

        /// <summary>
        /// Zapisywanie danych pacjenta w bazie danych
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonZapiszDanePacjenta_Click(object sender, RoutedEventArgs e) {
            try {
                e.Handled = ValidatePatientDataForm();
                if (e.Handled) {
                    // zapis
                    Pacjent pd = CreatePatientData();
                    
                    if (DbManager.PacjentRepository.Add(pd)) {

                        TabControlPacjenci.SelectedItem = TabItemPacjentList;
                        FillPatientDataForm(new Pacjent());
                        RefreshPacjentList();

                        MenuItemNotification.Header = "Pomyślnie zapisano dane pacjenta";

                        // wyczyszczenie info, że poszedł zapis
                        Task.Run(async () => {
                            await Task.Delay(10000);
                            Dispatcher.Invoke(() => {
                                MenuItemNotification.Header = "";
                            });
                        });
                    }
                    else {
                        // coś się popsuło, jakiś komunikat
                    }
                }
            }
            catch(Exception ex) {
                _log.Error(ex);
                // komunikat
            }
        }

        /// <summary>
        /// Odświeżenie listy pacjentów 
        /// </summary>
        private void RefreshPacjentList() {
            ListViewPacjenci.ItemsSource = null;
            ListViewPacjenci.ItemsSource = DbManager.PacjentRepository.FindAll();
        }

        private void RefreshPomiarList() {
            ListViewPomiary.ItemsSource = null;
            var pac = (ListViewPacjenci.SelectedItem as Pacjent);
            if (pac != null) {

                ListViewPomiary.ItemsSource = DbManager.BadanieRepository.FindByPacjentId(pac.Id);
            }
            
        }

        /// <summary>
        /// Zatrzymanie symulacji - może być ciekawie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void MenuZatrzymaj_Click(object sender, RoutedEventArgs e) {
            if(Logic != null) {

                // jakieś info, że trwa zatrzymywanie
                MenuItemNotification.Header = "Zatrzymywanie pomiaru...";

                bool stopped = false;
                while (!stopped) {
                    stopped = await Logic.StopSimulation();
                }
                if (stopped) {
                    Stopped = true;

                    // refresh zmiennych
                    RealTimeAnalysis = false;
                    RealTimeDataCalc = false;
                    ResultCompare = false;
                    IsSimulation = false;

                    // refresh GUI
                    Dispatcher.Invoke(() => {

                        // tutaj trzeba resetować gui - czyścić zaznaczenia, checkboxy, textFieldy, etc
                        SliderLewaNogaCzas.IsEnabled = true;
                        SliderLewaNogaCzas.Maximum = GlobalVariables.Data.TimeStamps.Last();
                        SliderPrawaNogaCzas.IsEnabled = true;

                        CheckBoxAnalizaDanych.IsChecked = false;
                        CheckBoxPodgladDanych.IsChecked = false;

                        ButtonDodajWykres.IsEnabled = true;
                        ButtonUsunWykres.IsEnabled = true;

                        // wyczyszczenie radio
                        RadioNowyPomiar.IsChecked = RadioNowyZPorownaniem.IsChecked = RadioWczytajZPliku.IsChecked = false;

                        TabItemMenu.IsEnabled = true;
                        ButtonRozpocznijBadanie.IsEnabled = false;


                        FillPatientDataForm(new Pacjent());
                        // ListViewPacjenci.SelectedItem = null;

                        // jeśli był wywoływany wątek z przeliczaniem danych to go zakończ
                        CalculateDataTimer?.Stop();
                        // i przelicz dane jeszcze raz
                        CalculateData();
                        MenuZatrzymaj.IsEnabled = false;
                    }, System.Windows.Threading.DispatcherPriority.Normal);

                    IsSimulation = false;

                    // odświeżenie wykresów
                    await Task.Run(() => {
                        Dispatcher.Invoke(GlobalVariables.ChartManager.RefreshAllCharts);
                    });

                    // zapis do plików i do bazy
                    SaveToZip();

                    // przeliczenie danych
                    
                }

                // await Logic.StopSimulation();
                await Task.Run(() => {
                    Task.Delay(1000);
                    Dispatcher.Invoke(() => {
                        MenuItemNotification.Header = "";
                    });
                });

            }

        }

        private void RangeSlider_ValueChanged(object sender, MahApps.Metro.Controls.RangeParameterChangedEventArgs e) {
            // przeliczenie danych
            var slider = sender as RangeSlider;
            double lower = slider.LowerValue;
            double upper = slider.UpperValue;

            GuiOperation(() => {
                switch (e.ParameterType) {
                    case RangeParameterChangeType.Lower:
                        // lower
                        if (slider.Name == "RangeSliderLewaNoga")
                            RangeSliderPrawaNoga.LowerValue = lower;
                        else
                            RangeSliderLewaNoga.LowerValue = lower;
                        break;
                    case RangeParameterChangeType.Upper:
                        // upper
                        if (slider.Name == "RangeSliderLewaNoga")
                            RangeSliderPrawaNoga.UpperValue = upper;
                        else
                            RangeSliderLewaNoga.UpperValue = upper;
                        break;
                }
            });

            //GuiOperation(() => {
            //    CalculateData(lower, upper);
            //});

        }

        private void ButtonPrzegladDanych_Click(object sender, RoutedEventArgs e) {
            // Wczytywanie danych z plików - czyli rozpakowanie, wczytanie, etc.
            // potrzebuję struktury pliku - najpierw przetestować, czy dane będą się odbierać, później zrobić zapis i
            // dopiero odczyt
            // 
        }

        #endregion

        #region Metody

        /// <summary>
        /// Wyszukiwanie kontrolek o podanym typie w określonym kontenerze. Uwaga - działa rekurencyjnie, przegląda
        /// również zagnieżdżone kontenery, dla większych struktur może działać wolno.
        /// </summary>
        /// <typeparam name="T">Typ szukanych kontrolek</typeparam>
        /// <typeparam name="K">Typ, w którym się szuka (czyli 'Panel')</typeparam>
        /// <param name="parent">Kontener, w którym się szuka kontrolek (Czyli jakiś panel, grid etc.)</param>
        /// <returns></returns>
        public static List<T> GetControls<T, K>(K parent) where K : Panel where T : Control {

            var controlList = parent.Children;
            var castedList = controlList.Cast<System.Windows.UIElement>();
            var returnList = new List<T>();

            // rekurencja

            foreach (var item in castedList) {
                if (item is Panel)
                    returnList.AddRange(GetControls<T, Panel>(item as Panel));
            }

            var whereList = castedList.Where(x => x is T);
            var selectedList = whereList.Select(x => x as T).ToList();

            returnList.AddRange(selectedList);

            return returnList;
        }

        /// <summary>
        /// Metoda otwiera okno wyboru pliku.
        /// Chyba się z tego zrezygnuje na rzecz bazy.
        /// </summary>
        /// <param name="extension">Rozszerzenie pliku, bez kropki na początku</param>
        /// <param name="name">Opis rozszerzenia, który pojawi się w oknie</param>
        /// <returns></returns>
        private Microsoft.Win32.OpenFileDialog OpenFileDialog(string extension, string name, string path = "") {
            Microsoft.Win32.OpenFileDialog fileDialog = new Microsoft.Win32.OpenFileDialog {
                DefaultExt = extension,
                Filter = $"{name}(*.{extension})|*.{extension}",
                InitialDirectory = string.IsNullOrEmpty(path) ? AppDomain.CurrentDomain.BaseDirectory : path,
                AddExtension = true,
                Multiselect = false, // wybór tylko jednego pliku
                CheckFileExists = true, // jeśli ktoś wklepie z palca nazwę, która nie istnieje to warto o tym poinformować
                CheckPathExists = true // jak wyżej, tylko chodzi o ścieżkę do pliku
            };

            bool? result = fileDialog.ShowDialog();

            if (result == true) {
                return fileDialog;
            }
            else {
                return null;
            }
        }

        /// <summary>
        /// Metoda wypełnia formularz danych pacjenta - czyli wszystkie kontrolki
        /// </summary>
        /// <param name="pd">Dane pacjenta</param>
        private void FillPatientDataForm(Pacjent pd) {
            // po kolei wpisywać do kontrolek
            TextBoxPacjentId.Text = pd.Id.ToString();
            TextBoxImie.Text = pd.Imie;
            TextBoxNazwisko.Text = pd.Nazwisko;
            TextBoxWiek.Text = pd.Wiek.ToString();

            TextBoxLewaStrzalka.Text = pd.LewaStrzalka.ToString();
            TextBoxLeweUdo.Text = pd.LeweUdo.ToString();
            TextBoxLewePrzedramie.Text = pd.LewePrzedramie.ToString();
            TextBoxLeweRamie.Text = pd.LeweRamie.ToString();

            TextBoxPrawaStrzalka.Text = pd.PrawaStrzalka.ToString();
            TextBoxPraweUdo.Text = pd.PraweUdo.ToString();
            TextBoxPrawePrzedramie.Text = pd.PrawePrzedramie.ToString();
            TextBoxPraweRamie.Text = pd.PraweRamie.ToString();

            TextBoxBarki.Text = pd.Klatka.ToString();
            TextBoxMiednica.Text = pd.Miednica.ToString();

            TextBoxWzrost.Text = pd.Wzrost.ToString();
        }

        /// <summary>
        /// Tworzenie obiektu 'PatientData' na podstawie formularza w aplikacji
        /// </summary>
        /// <returns></returns>
        private Pacjent CreatePatientData() {
            Pacjent pd = new Pacjent();

            int.TryParse(TextBoxPacjentId.Text, out int id);
            pd.Id = id;

            pd.Imie = TextBoxImie.Text;
            pd.Nazwisko = TextBoxNazwisko.Text;
            pd.Wiek = int.Parse(TextBoxWiek.Text);
            pd.Wzrost = int.Parse(TextBoxWzrost.Text);

            pd.LewaStrzalka = double.Parse(TextBoxLewaStrzalka.Text);
            pd.LeweUdo = double.Parse(TextBoxLeweUdo.Text);
            pd.LewePrzedramie = double.Parse(TextBoxLewePrzedramie.Text);
            pd.LeweRamie = double.Parse(TextBoxLeweRamie.Text);

            pd.PrawaStrzalka = double.Parse(TextBoxPrawaStrzalka.Text);
            pd.PraweUdo = double.Parse(TextBoxPraweUdo.Text);
            pd.PrawePrzedramie = double.Parse(TextBoxPrawePrzedramie.Text);
            pd.PraweRamie = double.Parse(TextBoxPraweRamie.Text);

            pd.Klatka = double.Parse(TextBoxBarki.Text);
            pd.Miednica = double.Parse(TextBoxMiednica.Text);

            return pd;
        }

        /// <summary>
        /// Metoda sprawdza, czy podany CheckBox jest zaznaczony
        /// </summary>
        /// <param name="sender"></param>
        /// <returns></returns>
        private bool IsChecked(object sender) {
            return ((sender is CheckBox cb) ? (cb.IsChecked ?? false) : false);
        }

        /// <summary>
        /// Realizacja operacji na GUI w sposób bezpieczny wątkowo
        /// </summary>
        /// <param name="action">Metoda do wywołania, która modyfikuje kontrolki</param>
        /// <param name="priority">Priorytet operacji, domyślnie 'Normal'</param>
        public void GuiOperation(Action action, System.Windows.Threading.DispatcherPriority priority = System.Windows.Threading.DispatcherPriority.Normal) {
            try {
                Dispatcher.Invoke(action, priority);
            }
            catch (Exception ex) {
                _log.Error(ex);
            }
        }


        /// <summary>
        /// Obliczanie maksimum, minimum, średnich z danych.
        /// URUCHAMIAĆ W DISPATCHERZE!!! Inaczej będzie walić wyjątkami na lewo i prawo!
        /// Domyślnie powinno odpalać się co sekundę i po kliknieciu 'Zatrzymaj'
        /// Jak zrealizować liczenie w zakresie czasowym? GetRange?
        /// </summary>
        private void CalculateData(double startTime = 0, double endTime = 0) {
           
            lock (GlobalVariables.DataLocker) {
                // koniec i początek zakresu
                int start = 0, end = 0, count = 0;

                // jeżeli czas końca to cały zakres i elo, bez zabaw
                if(endTime == 0) {
                    end = GlobalVariables.Data.TimeStamps.Count - 1;
                }
                else {
                    start = GlobalVariables.Data.TimeStamps.IndexOf(GlobalVariables.Data.TimeStamps.Where(x => x <= startTime).Max());
                    end = GlobalVariables.Data.TimeStamps.IndexOf(GlobalVariables.Data.TimeStamps.Where(x => x <= endTime).Max());
                }

                count = end - start + 1;

                if (count == 0) return;

                // LEWA NOGA
                // Kąt zgięcia
                //
                TextBoxLewaNogaKKostkaMin.Text = GlobalVariables.Data.LeftFeetAngle.MinStr(start, count);
                TextBoxLewaNogaKKostkaSr.Text = GlobalVariables.Data.LeftFeetAngle.AverageStr(start, count);
                TextBoxLewaNogaKKostkaMax.Text = GlobalVariables.Data.LeftFeetAngle.MaxStr(start, count);

                TextBoxLewaNogaKKolanoMin.Text = GlobalVariables.Data.LeftKneeAngle.MinStr(start, count);
                TextBoxLewaNogaKKolanoSr.Text = GlobalVariables.Data.LeftKneeAngle.AverageStr(start, count);
                TextBoxLewaNogaKKolanoMax.Text = GlobalVariables.Data.LeftKneeAngle.MaxStr(start, count);

                TextBoxLewaNogaKBiodroMin.Text = GlobalVariables.Data.LeftHipAngle.MinStr(start, count);
                TextBoxLewaNogaKBiodroSr.Text = GlobalVariables.Data.LeftHipAngle.AverageStr(start, count);
                TextBoxLewaNogaKBiodroMax.Text = GlobalVariables.Data.LeftHipAngle.MaxStr(start, count);

                // Odwiedzenie
                //
                TextBoxLewaNogaOKostkaMin.Text = GlobalVariables.Data.LeftFeetAdduction.MinStr(start, count);
                TextBoxLewaNogaOKostkaSr.Text = GlobalVariables.Data.LeftFeetAdduction.AverageStr(start, count);
                TextBoxLewaNogaOKostkaMax.Text = GlobalVariables.Data.LeftFeetAdduction.MaxStr(start, count);

                TextBoxLewaNogaOKolanoMin.Text = GlobalVariables.Data.LeftKneeAdduction.MinStr(start, count);
                TextBoxLewaNogaOKolanoSr.Text = GlobalVariables.Data.LeftKneeAdduction.AverageStr(start, count);
                TextBoxLewaNogaOKolanoMax.Text = GlobalVariables.Data.LeftKneeAdduction.MaxStr(start, count);

                TextBoxLewaNogaOBiodroMin.Text = GlobalVariables.Data.LeftHipAdduction.MinStr(start, count);
                TextBoxLewaNogaOBiodroSr.Text = GlobalVariables.Data.LeftHipAdduction.AverageStr(start, count);
                TextBoxLewaNogaOBiodroMax.Text = GlobalVariables.Data.LeftHipAdduction.MaxStr(start, count);

                // Rotacja
                //
                TextBoxLewaNogaRKostkaMin.Text = GlobalVariables.Data.LeftFeetRotation.MinStr(start, count);
                TextBoxLewaNogaRKostkaSr.Text = GlobalVariables.Data.LeftFeetRotation.AverageStr(start, count);
                TextBoxLewaNogaRKostkaMax.Text = GlobalVariables.Data.LeftFeetRotation.MaxStr(start, count);

                TextBoxLewaNogaRKolanoMin.Text = GlobalVariables.Data.LeftKneeRotation.MinStr(start, count);
                TextBoxLewaNogaRKolanoSr.Text = GlobalVariables.Data.LeftKneeRotation.AverageStr(start, count);
                TextBoxLewaNogaRKolanoMax.Text = GlobalVariables.Data.LeftKneeRotation.MaxStr(start, count);

                TextBoxLewaNogaRBiodroMin.Text = GlobalVariables.Data.LeftHipRotation.MinStr(start, count);
                TextBoxLewaNogaRBiodroSr.Text = GlobalVariables.Data.LeftHipRotation.AverageStr(start, count);
                TextBoxLewaNogaRBiodroMax.Text = GlobalVariables.Data.LeftHipRotation.MaxStr(start, count);

                // KROKI - LEWA NOGA
                // 
                var krokiLewa = GlobalVariables.Data.LeftLegSteps.Where(x => x.EndIteration.HasValue && x.EndIteration >= start && x.StartIteration <= end);
                if (krokiLewa.Any()) {
                    // Długość kroku
                    //
                    TextBoxLewaNogaDlKrokuMin.Text = krokiLewa.Min(x => x.Length).ToString("F1");
                    TextBoxLewaNogaDlKrokuSr.Text = krokiLewa.Average(x => x.Length).ToString("F1");
                    TextBoxLewaNogaDlKrokuMax.Text = krokiLewa.Max(x => x.Length).ToString("F1");

                    // Czas kroku
                    //
                    var czasy = krokiLewa.Select(x => x.EndTime.Value - x.StartTime);
                    TextBoxLewaNogaCzKrokuMin.Text = czasy.Min().ToString("F1");
                    TextBoxLewaNogaCzKrokuSr.Text = czasy.Average().ToString("F1");
                    TextBoxLewaNogaCzKrokuMax.Text = czasy.Max().ToString("F1");
                    
                }

                if(GlobalVariables.SensorManager.SensorList.Length> 4) {
                    // PRAWA NOGA
                    // Kąt zgięcia
                    //
                    TextBoxPrawaNogaKKostkaMin.Text = GlobalVariables.Data.RightFeetAngle.MinStr(start, count);
                    TextBoxPrawaNogaKKostkaSr.Text = GlobalVariables.Data.RightFeetAngle.AverageStr(start, count);
                    TextBoxPrawaNogaKKostkaMax.Text = GlobalVariables.Data.RightFeetAngle.MaxStr(start, count);

                    TextBoxPrawaNogaKKolanoMin.Text = GlobalVariables.Data.RightKneeAngle.MinStr(start, count);
                    TextBoxPrawaNogaKKolanoSr.Text = GlobalVariables.Data.RightKneeAngle.AverageStr(start, count);
                    TextBoxPrawaNogaKKolanoMax.Text = GlobalVariables.Data.RightKneeAngle.MaxStr(start, count);

                    TextBoxPrawaNogaKBiodroMin.Text = GlobalVariables.Data.RightHipAngle.MinStr(start, count);
                    TextBoxPrawaNogaKBiodroSr.Text = GlobalVariables.Data.RightHipAngle.AverageStr(start, count);
                    TextBoxPrawaNogaKBiodroMax.Text = GlobalVariables.Data.RightHipAngle.MaxStr(start, count);

                    // Odwiedzenie
                    //
                    TextBoxPrawaNogaOKostkaMin.Text = GlobalVariables.Data.RightFeetAdduction.MinStr(start, count);
                    TextBoxPrawaNogaOKostkaSr.Text = GlobalVariables.Data.RightFeetAdduction.AverageStr(start, count);
                    TextBoxPrawaNogaOKostkaMax.Text = GlobalVariables.Data.RightFeetAdduction.MaxStr(start, count);

                    TextBoxPrawaNogaOKolanoMin.Text = GlobalVariables.Data.RightKneeAdduction.MinStr(start, count);
                    TextBoxPrawaNogaOKolanoSr.Text = GlobalVariables.Data.RightKneeAdduction.AverageStr(start, count);
                    TextBoxPrawaNogaOKolanoMax.Text = GlobalVariables.Data.RightKneeAdduction.MaxStr(start, count);

                    TextBoxPrawaNogaOBiodroMin.Text = GlobalVariables.Data.RightHipAdduction.MinStr(start, count);
                    TextBoxPrawaNogaOBiodroSr.Text = GlobalVariables.Data.RightHipAdduction.AverageStr(start, count);
                    TextBoxPrawaNogaOBiodroMax.Text = GlobalVariables.Data.RightHipAdduction.MaxStr(start, count);

                    // Rotacja
                    //
                    TextBoxPrawaNogaRKostkaMin.Text = GlobalVariables.Data.RightFeetRotation.MinStr(start, count);
                    TextBoxPrawaNogaRKostkaSr.Text = GlobalVariables.Data.RightFeetRotation.AverageStr(start, count);
                    TextBoxPrawaNogaRKostkaMax.Text = GlobalVariables.Data.RightFeetRotation.MaxStr(start, count);

                    TextBoxPrawaNogaRKolanoMin.Text = GlobalVariables.Data.RightKneeRotation.MinStr(start, count);
                    TextBoxPrawaNogaRKolanoSr.Text = GlobalVariables.Data.RightKneeRotation.AverageStr(start, count);
                    TextBoxPrawaNogaRKolanoMax.Text = GlobalVariables.Data.RightKneeRotation.MaxStr(start, count);

                    TextBoxPrawaNogaRBiodroMin.Text = GlobalVariables.Data.RightHipRotation.MinStr(start, count);
                    TextBoxPrawaNogaRBiodroSr.Text = GlobalVariables.Data.RightHipRotation.AverageStr(start, count);
                    TextBoxPrawaNogaRBiodroMax.Text = GlobalVariables.Data.RightHipRotation.MaxStr(start, count);

                    // KROKI - PRAWA NOGA
                    // 
                    var krokiPrawa = GlobalVariables.Data.RightLegSteps.Where(x => x.EndIteration.HasValue && x.EndIteration >= start && x.StartIteration <= end);
                    if (krokiPrawa.Any()) {
                        // Długość kroku
                        //
                        TextBoxPrawaNogaDlKrokuMin.Text = krokiPrawa.Min(x => x.Length).ToString("F1");
                        TextBoxPrawaNogaDlKrokuSr.Text = krokiPrawa.Average(x => x.Length).ToString("F1");
                        TextBoxPrawaNogaDlKrokuMax.Text = krokiPrawa.Max(x => x.Length).ToString("F1");

                        // Czas kroku
                        //
                        var czasy = krokiPrawa.Select(x => x.EndTime.Value - x.StartTime);
                        TextBoxPrawaNogaCzKrokuMin.Text = czasy.Min().ToString("F1");
                        TextBoxPrawaNogaCzKrokuSr.Text = czasy.Average().ToString("F1");
                        TextBoxPrawaNogaCzKrokuMax.Text = czasy.Max().ToString("F1");

                    }
                }

                // MIEDNICA - WSPÓLNE
                // Pochylenie przód/tył
                //
                TextBoxLewaNogaKMiednicaMin.Text = TextBoxPrawaNogaKMiednicaMin.Text = GlobalVariables.Data.PelvicAngle.MinStr(start, count);
                TextBoxLewaNogaKMiednicaSr.Text = TextBoxPrawaNogaKMiednicaSr.Text = GlobalVariables.Data.PelvicAngle.AverageStr(start, count);
                TextBoxLewaNogaKMiednicaMax.Text = TextBoxPrawaNogaKMiednicaMax.Text = GlobalVariables.Data.PelvicAngle.MaxStr(start, count);

                // Pochylenie boczne
                //
                TextBoxLewaNogaOMiednicaMin.Text = TextBoxPrawaNogaOMiednicaMin.Text = GlobalVariables.Data.PelvicAdduction.MinStr(start, count);
                TextBoxLewaNogaOMiednicaSr.Text = TextBoxPrawaNogaOMiednicaSr.Text = GlobalVariables.Data.PelvicAdduction.AverageStr(start, count);
                TextBoxLewaNogaOMiednicaMax.Text = TextBoxPrawaNogaOMiednicaMax.Text = GlobalVariables.Data.PelvicAdduction.MaxStr(start, count);

                // Rotacja
                //
                TextBoxLewaNogaRMiednicaMin.Text = TextBoxPrawaNogaRMiednicaMin.Text = GlobalVariables.Data.PelvicRotation.MinStr(start, count);
                TextBoxLewaNogaRMiednicaSr.Text = TextBoxPrawaNogaRMiednicaSr.Text = GlobalVariables.Data.PelvicRotation.AverageStr(start, count);
                TextBoxLewaNogaRMiednicaMax.Text = TextBoxPrawaNogaRMiednicaMax.Text = GlobalVariables.Data.PelvicRotation.MaxStr(start, count);



                // CYKLE - WSPÓLNE
                //
                // będzie niewrażliwe na zmiany czasu - przynajmniej tymczasowo

                var lewa = GlobalVariables.Data.LeftLegSteps.Where(x => x.IsCompleted).ToList();
                var prawa = GlobalVariables.Data.RightLegSteps.Where(x => x.IsCompleted).ToList();

                // powinno być niewrażliwe na liczbę czujników
                if (lewa.Any() && prawa.Any()) {
                    // Czas cyklu
                    //
                    // cykl zaczął się lewą nogą
                    List<double> czasy = new List<double>();
                    List<double> podpor = new List<double>();
                    List<double> przen = new List<double>();
                    //  List<double> 
                    if (lewa.First().StartTime < prawa.First().StartTime) {
                        for (int i = 0; i < lewa.Count; i++) {
                            if (prawa.Count > i) {
                                czasy.Add(prawa[i].EndTime.Value - lewa[i].StartTime);
                                podpor.Add(prawa[i].StartTime - lewa[i].EndTime.Value);
                                przen.Add(czasy[i] - podpor[i]);
                            }
                        }
                    }
                    else {
                        // cykl zaczął się prawą nogą
                        for (int i = 0; i < prawa.Count; i++) {
                            if (lewa.Count > i) {
                                czasy.Add(lewa[i].EndTime.Value - prawa[i].StartTime);
                                podpor.Add(lewa[i].StartTime - prawa[i].EndTime.Value);
                                przen.Add(czasy[i] - podpor[i]);
                            }
                        }
                    }

                    TextBoxLewaNogaCzCykluMin.Text = TextBoxPrawaNogaCzCykluMin.Text = czasy.Min().ToString("F1");
                    TextBoxLewaNogaCzCykluSr.Text = TextBoxLewaNogaCzCykluSr.Text = czasy.Average().ToString("F1");
                    TextBoxLewaNogaCzCykluMax.Text = TextBoxPrawaNogaCzCykluMax.Text = czasy.Max().ToString("F1");

                    // Przenoszenie
                    //
                    TextBoxLewaNogaPrzMin.Text = TextBoxPrawaNogaPrzMin.Text = przen.Min().ToString("F1");
                    TextBoxLewaNogaPrzSr.Text = TextBoxPrawaNogaPrzSr.Text = przen.Average().ToString("F1");
                    TextBoxLewaNogaPrzMax.Text = TextBoxPrawaNogaPrzMax.Text = przen.Max().ToString("F1");
                    TextBoxLewaNogaPrzPr.Text = TextBoxPrawaNogaPr.Text = $"{(przen.Sum() * 100 / czasy.Sum()):F2}%";

                    // Podpór pełny
                    //
                    TextBoxLewaNogaPodPelMin.Text = TextBoxPrawaNogaPodPelMin.Text = podpor.Min().ToString("F1");
                    TextBoxLewaNogaPodPelSr.Text = TextBoxPrawaNogaPodPelSr.Text = podpor.Average().ToString("F1");
                    TextBoxLewaNogaPodPelMax.Text = TextBoxPrawaNogaPodPelMax.Text = podpor.Max().ToString("F1");
                    TextBoxLewaNogaPodPelPr.Text = TextBoxPrawaNogaPodPelPr.Text = $"{(podpor.Sum() * 100 / czasy.Sum()):F2}%";
                }
            }
        }



        #endregion

        private async void Window_Loaded(object sender, RoutedEventArgs e) {
            await Task.Run(() => {
                Dispatcher.Invoke(() => {
                    TabControlMain.SelectedItem = TabItemLewaWykresy;
                });
                Task.Delay(100);
                Dispatcher.Invoke(() => {
                    TabControlMain.SelectedItem = TabItemPrawaWykresy;
                });
                Task.Delay(100);
                Dispatcher.Invoke(() => {
                    TabControlMain.SelectedItem = TabItemWykresyTestowe;
                });
                Task.Delay(100);
                Dispatcher.Invoke(() => {
                    TabControlMain.SelectedItem = TabItemMenu;
                });
                Task.Delay(100);

            });
        }

        /// <summary>
        /// Zapisywanie pomiaru - pytanie, czy zapis do bazy zrobić tutaj, czy na zewnątrz? Chyba tutaj
        /// </summary>
        private async void SaveToZip() {
            if(Logic?.IsThreadWorking ?? false) {
                await Task.Run(() => {
                    Dispatcher.Invoke(() => {
                        MessageBox.Show("Musisz zatrzymać pomiar aby zapisać dane!");
                    });
                });
                return;
            }
            if (Logic != null) {
                await Task.Run(() => {
                    string path = "../../Wyniki";
                    if (!Directory.Exists(path)) Directory.CreateDirectory(path);
                    path += "/";

                    string filename = $"Wyniki_{GlobalVariables.PatientData?.Imie}_{GlobalVariables.PatientData?.Nazwisko}_{DateTime.Now.ToString("yyyy_MM_dd_hh_mm")}.zip";
                    ZipArchive zip = ZipFile.Open(path + filename, ZipArchiveMode.Create);
                    
                    for (int i = 0; i < GlobalVariables.SensorManager.SensorList.Length; i++) {
                        ZipArchiveEntry zae = zip.CreateEntry($"log_{i + 1}.csv");
                        StreamWriter sw = new StreamWriter(zae.Open());
                        GlobalVariables.SensorManager.SensorList[i].WriteToStream(sw);

                    }

                    // tutaj - zapis w bazie
                    // jeśli nie ma pacjenta - to bez zapisu w bazie
                    Model.Badanie b = new Badanie() {
                        PacjentId = GlobalVariables.PatientData?.Id ?? -1,
                        Plik = filename,
                        SciezkaDoPliku = path + filename
                    };

                    Dispatcher.Invoke(() => {
                        InputDialog inDialog = new InputDialog();
                        bool? showed = inDialog.ShowDialog();
                        if (inDialog.IsOk) {
                            b.Notatka = inDialog.Notatka;
                        }
                    }, System.Windows.Threading.DispatcherPriority.Normal);

                    if (DbManager.BadanieRepository.Add(b)) {
                        // jakieś info, że przeszło
                        Dispatcher.Invoke(() => {
                            RefreshPacjentList();
                        });
                    }

                    zip.Dispose();
                });


            }
        }

        /// <summary>
        /// Modyfikacja danych pacjenta - trzeba dodać gdzieś pole z id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e) {
            FillPatientDataForm(new Pacjent());
            TabControlPacjenci.SelectedItem = TabItemPacjentList;
        }

        /// <summary>
        /// Tworzenie nowego pacjenta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonNowyPacjent_Click(object sender, RoutedEventArgs e) {
            FillPatientDataForm(new Pacjent());
            TabControlPacjenci.SelectedItem = TabItemPacjentForm;
        }

        private void ListViewPacjenci_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var list = sender as ListView;
            var selected = list.SelectedItem as Pacjent;
            
            // bez sprawdzenia czy jest null
            PatientData = selected;
            ButtonModyfikujPacjenta.IsEnabled = ButtonUsunPacjenta.IsEnabled = selected != null;
            EnableDisable_ButtonRozpocznij();
            RefreshPomiarList();
        }

        private void ButtonModyfikujPacjenta_Click(object sender, RoutedEventArgs e) {
            var selected = ListViewPacjenci.SelectedItem as Pacjent;
            if(selected != null) {
                FillPatientDataForm(selected);
                TabControlPacjenci.SelectedItem = TabItemPacjentForm;
            }
            else {

            }
        }

        private void ButtonUsunPacjenta_Click(object sender, RoutedEventArgs e) {
            var selected = ListViewPacjenci.SelectedItem as Pacjent;
            if(selected != null) {
                if(selected.Equals(GlobalVariables.PatientData))
                    GlobalVariables.PatientData = null;
                PatientData = null;
                DbManager.PacjentRepository.Remove(selected);
                RefreshPacjentList();
            }
        }

        /// <summary>
        /// Zaznaczenie radio z trybem pracy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Radio_Checked(object sender, RoutedEventArgs e) {
            var checkedRadio = sender as RadioButton;

            EnableDisable_ButtonRozpocznij();
            
        }

        private bool IsWorkModeChosen() {
            return (RadioNowyPomiar.IsChecked ?? false) || 
                    (RadioNowyZPorownaniem.IsChecked ?? false) || 
                    (RadioWczytajZPliku.IsChecked ?? false);
        }

        private void EnableDisable_ButtonRozpocznij() {
            if (IsWorkModeChosen() && PatientData != null && ListViewPacjenci.SelectedItem != null) {
                if (RadioNowyPomiar.IsChecked ?? false) {
                    ButtonRozpocznijBadanie.IsEnabled = true;
                }
                else {
                    ButtonRozpocznijBadanie.IsEnabled = (ListViewPomiary.SelectedItem != null);
                }
            }
            else {
                ButtonRozpocznijBadanie.IsEnabled = false;
            }
        }

        private void ListViewPomiary_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            EnableDisable_ButtonRozpocznij();
        }

        private void TabControlPacjenci_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            // ustawienie wymiaru

            if(TabControlPacjenci.SelectedItem == TabItemPacjentForm) {
                ListViewPomiary.Height = 190;
            }
            else {
                ListViewPomiary.Height = 342;
            }
        }
        
    }
}
