﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class Logic {

        private static readonly ILog _log = LogManager.GetLogger("");

        private static Logic _instance;
        public static Logic Instance{
            get {
                if (_instance == null)
                    _instance = new Logic();
                return _instance;
            }
            private set { _instance = value; }
        }
        private Logic() { }


        // wire & wireless
        private Thread Receiver;
        public bool StopReceiving {
            get;
            set;
        }

        public bool IsThreadWorking => Receiver?.IsAlive ?? false;

        // wire
        // private byte[] _Ip;

        // wireless
        private SerialPort _serialPort;
        private string _comName;

        private bool _simulation;
        private SensorSimulate _sensorSimulate;

        private void PreapareSimulation(bool simulate) {
            if (simulate) {
                _simulation = true;
                _sensorSimulate = new SensorSimulate();
                _sensorSimulate.Interval = 100;
                _sensorSimulate.SensorCount = 4;
            }
        }

        public void BeginSimulation(bool simulate = false) {
            StopReceiving = false;
            PreapareSimulation(simulate);
            switch (GlobalVariables.SimulationType) {
                case Type.Wire:
                    break;
                case Type.Wireless:
                    if (GlobalVariables.IsCompare)
                        LoadFromFiles();
                    else
                        PrepareThreadWireless();
                    break;

                case Type.File:
                    // przydałby się uchwyt do Zipa z danymi w GlobalVariables
                    LoadFromFiles();
                    break;
            }
        }

        private void PrepareThreadWireless() {

            try {
                _comName = "COM4";

                //InitWireless();
                //_serialPort.Open();

                Receiver = new Thread(ReceiveWireless);
                Receiver.Priority = ThreadPriority.AboveNormal;
                GlobalVariables.StartT = DateTime.Now;
                Receiver.Start();
            }
            catch(Exception ex) {
                _log.Error(ex);
            }
        }

        private void ReceiveWireless(){
            try {
                // tymczasowo
                if (!_simulation) {
                    _comName = "COM4";

                    InitWireless();
                    _serialPort.Open();
                    _serialPort.DiscardInBuffer();
                }

                List<DataRow> dataRows = new List<DataRow>();

                double actTime = 0; // czas w ms
                double prevTime = 0; // czas w ms
                DateTime startTime = DateTime.MinValue;


                while (!StopReceiving) {
                    if (!_simulation) {
                        while (_serialPort.BytesToRead == 0 && !StopReceiving) {
                            Thread.Sleep(1);
                        }
                    }
                    

                    int newByte = (_simulation) ? _sensorSimulate.ReadByte() : _serialPort.ReadByte();

                    if (startTime == DateTime.MinValue) {
                        startTime = DateTime.Now;
                    }

                    if (newByte == -1) {
                        // koniec bufora - return?
                        return;
                    }
                    // GlobalVariables.ChartManager.DiagSensorID = 1;
                    byte newOne = (byte)newByte;

                    if (newOne == GlobalVariables.RS_SOP) {
                        // lista zawiera początek ramki
                        List<byte> buffer = new List<byte>() { newOne };

                        do {
                            newOne = (_simulation) ? _sensorSimulate.ReadByte() : (byte)_serialPort.ReadByte();
                            buffer.Add(newOne);
                        }
                        while (newOne != GlobalVariables.RS_EOP);

                        // parsowanie do DataRow
                        // gdzie usuwanie znaku specjalnego z inputu?
                        var newDataRow = new DataRow(buffer.ToArray(), actTime, prevTime);

                        if(newDataRow.SensorId > 8) {
                            foreach(var d in buffer) { Console.Write($"{d} "); }
                            Console.WriteLine();
                        }

                        if (dataRows.Count > 0 && dataRows.Last().Time != newDataRow.Time) {
                            //Console.WriteLine($"Received {dataRows.Count} datarows");

                            // Jakby w globalVariables (testowo) id czujnika, z którego odebrano dane
                            // i w chart manager insertować te dane do wykresów
                            Console.WriteLine($"GlobalVariables.SensorManager.ReceiveWireless(dataRows), InsertedCount: {GlobalVariables.InsertedCount}" +
                                $", ActCount: {GlobalVariables.Data?.LeftFeetAngle?.Count}");
                            GlobalVariables.SensorManager.ReceiveWireless(dataRows);

                            // logika niżej powinna być ok, z naciskiem na 'powinna' - sprawdzić na spokojnie
                            Console.WriteLine($"GlobalVariables.CalculationManager.CalculateAngles(), InsertedCount: {GlobalVariables.InsertedCount}" +
                                $", ActCount: {GlobalVariables.Data?.LeftFeetAngle?.Count}");
                            GlobalVariables.CalculationManager.CalculateAngles(); // chwilowo olewam, sprawdzam czy dane wogóle będą się insertować na wykresy
                            // GlobalVariables.SensorManager.CheckState();

                            Console.WriteLine($"GlobalVariables.ChartManager.InsertData(), InsertedCount: {GlobalVariables.InsertedCount}, " +
                                $"ActCount: {GlobalVariables.Data?.LeftFeetAngle?.Count}");
                            GlobalVariables.ChartManager.InsertData();
                            // GlobalVariables.ChartManager.InsertDataToDiagnosticCharts();
                            // czyszczenie listy
                            dataRows.Clear();

                            dataRows.Add(newDataRow);

                            // update czasu
                            prevTime = actTime;
                            actTime = (DateTime.Now - startTime).TotalSeconds;
                            newDataRow.ActTime = actTime;
                            newDataRow.PrevTime = prevTime;
                        }
                        else {
                            dataRows.Add(newDataRow);
                            continue;
                        }
                    }
                    else {
                        Console.WriteLine(newOne);
                        continue;
                    }
                }
            }
            catch (Exception ex) {
                _log.Error($"{ex}\n {ex.Data}");

            }
        }

        private void InitWireless() {
            _serialPort = new SerialPort();
            _serialPort.PortName = _comName;
            _serialPort.BaudRate = 115200;
            _serialPort.Parity = Parity.None;
            _serialPort.DataBits = 8;
            _serialPort.StopBits = StopBits.One;
            _serialPort.Handshake = Handshake.None;
            _serialPort.RtsEnable = false;
        }

        public async Task<bool> StopSimulation() {
            return await Task<bool>.Run(() => {
                StopReceiving = true;
                Task.Delay(200);
                if (Receiver.IsAlive) {
                    Receiver.Abort();
                    Task.Delay(100);
                }
                return !Receiver.IsAlive;
            });
        }

        private void LoadFromFiles() {
            // GlobalVariables.
            // Thread.Sleep(10000);

            GlobalVariables.SensorManager.LoadFromFiles();
            GlobalVariables.CalculationManager.CalculateAngles();


            // GlobalVariables.ChartManager.InsertData();
            GlobalVariables.ChartManager.InsertDataFromFiles();

            // jeszcze umieszenie danych w textboxach
            // no i analiza - to po stronie UI
        }

    }
}
