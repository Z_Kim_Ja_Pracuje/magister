﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class SensorManager
    {
        // log
        private static readonly ILog _log = LogManager.GetLogger("");

        private Sensor[] _sensorList;
        private List<int> _prevSetted = new List<int>();

        private Sensor[] _compareList;

        public Sensor[] SensorList {
            get { return _sensorList; }
            private set { _sensorList = value; }
        }
        
        public SensorManager() : this(new Sensor[26]) { }

        public SensorManager(Sensor[] _sensors) {
            SensorList = _sensors;
            ActTime = PrevTime = 0.0;
        }

        // czas - aktualny i w poprz iteracji [s]
        private double ActTime { get; set; }
        private double PrevTime { get; set; }
        public Sensor[] CompareList {
            get => _compareList;
            set => _compareList = value;
        }

        

        /// <summary>
        /// Wpisywanie danych do tablic z czujnikami.
        /// </summary>
        /// <param name="dataRows"></param>
        public void ReceiveWireless(List<DataRow> dataRows) {
            PrevTime = ActTime;
            ActTime = (DateTime.Now - GlobalVariables.StartT).TotalSeconds;

            // przygotowanie pod mniejszą ilośc czujników
            List<int> setted = new List<int>();
            foreach(var row in dataRows) {
                // Console.WriteLine($"SensorId = {row.SensorId}");
                // coś jest nie tak z tymi czujnikami - czasem wywalają takie śmieci, że łeb mały
                if (SensorList.Length < row.SensorId) continue;
                SensorList[row.SensorId - 1].ReceiveData(row);
                setted.Add(row.SensorId - 1);
                
                if (dataRows.Count == 1)
                    GlobalVariables.ChartManager.DiagSensorID = row.SensorId - 1;
            }

            Console.WriteLine(dataRows.Select(x => x.SensorId.ToString()).Aggregate((bef, aft) => $"{bef}, {aft}"));

            // wypisać te, które były przypisane - szukać błędu z symulacją

            for(int i = 0; i < SensorList.Length; i++) {
                if (!setted.Contains(i)) {
                    // jeżeli poprzednio były dodawane dane to je skopiuj
                    if (_prevSetted.Contains(i) && SensorList[i].Data.Count > 0) {
                        SensorList[i].ReceiveData(SensorList[i].Data.Last().Copy());
                    }
                    else {
                        SensorList[i].ReceiveEmpty();
                    }
                }
            }

            _prevSetted.Clear();
            _prevSetted.AddRange(setted);

            GlobalVariables.InsertedCount++;
            
            // powinno załatwić sprawę ale nie wiem, czy nie będzie trzeba locka
            GlobalVariables.Data.TimeStamps.Add(ActTime);
        }


        /// <summary>
        /// Sprawdzenie stanu czujników - czy mają odebrane dane
        /// </summary>
        public void CheckState() {

            // najlepiej zostawić puste i olać
        }

        public void SetCompareSensorList() {
            CompareList = new Sensor[SensorList.Length];
            for(int i = 0; i < CompareList.Length; i++) {
                CompareList[i] = new Sensor();
            }

            GlobalVariables.CalculationManager.CompareList = CompareList;
        }

        /// <summary>
        /// Metoda dla danych z plików
        /// Bawić się w odczytywanie po jednym wierszu czy jednak przesłać cały stream i wpierdalać pliki po kolei do Sensor[] ?
        /// </summary>
        /// <param name="data">Dane</param>
        //private void InsertData(string data)
        //{
        //    if(GlobalVariables.SimulationType == Type.Wireless) {
                
        //    }
        //}

        public void LoadFromFiles() {

            var destination = (GlobalVariables.IsCompare ? CompareList : SensorList);
            var entries = GlobalVariables.Zip?.Entries;

            if (entries.FirstOrDefault(x => x.Name.EndsWith(".xml")) /*stare czujniki*/ != null)
                SensorMappings.IsWirelessSensorMap = false;
            else
                SensorMappings.IsWirelessSensorMap = true;


            foreach (var entry in entries) {
                int.TryParse(entry.Name.Split('_')[1].Replace(".csv", ""), out int id);
                if(id > 0) {
                    int index = GetSensorIndex(id);
                    if(index >=0 && index < destination.Length)
                        destination[index].LoadFromFile(entry.Open());
                }
            }
            
            var target = (!GlobalVariables.IsCompare ? GlobalVariables.Data : GlobalVariables.DataCompare);
            
            target.LoadTimeVector(entries.FirstOrDefault(x=>x.Name.EndsWith(".csv"))?.Open());

        }

        public int GetSensorId(SensorsLocations location) {
            return SensorMappings.GetSensorId(location) - 1;
        }

        public int GetSensorIndex(int id) {
            return SensorMappings.GetSensorIndex(id);
        }

        public DataRow GetSensorValue(int counter, SensorsLocations location) {
            return GetSensorList()[GetSensorId(location)].GetValue(counter);
        }

        private Sensor[] GetSensorList() {
            return (!GlobalVariables.IsCompare ? SensorList : CompareList);
        }

    }

    public enum SensorsLocations {
        LeftFoot = 1,
        LeftKnee = 2,
        LeftHip = 3,
        Pelvic = 4,
        RightFoot = 5,
        RightKnee = 6,
        RightHip = 7
    }


    /// <summary>
    /// Klasa wykorzystywana do ustalenia mapowania czujników - które czujniki, gdzie dane zostały wczytane z plików, mają być zastosowane w obliczeniach
    /// </summary>
    public static class SensorMappings {
        /// <summary>
        /// Na razie domyślnie true, jak reszta będzie gotow ato zmienic
        /// </summary>
        public static bool IsWirelessSensorMap { get; set; } = true;

        public static Dictionary<SensorsLocations, int> WirelessSensorsMap = new Dictionary<SensorsLocations, int>() 
        { { SensorsLocations.LeftFoot, 1 }, { SensorsLocations.LeftKnee, 2 }, { SensorsLocations.LeftHip, 3 }, { SensorsLocations.Pelvic, 4 } };

        public static Dictionary<SensorsLocations, int> WireSensorMap = new Dictionary<SensorsLocations, int>() {
            {SensorsLocations.LeftFoot, 17 },
            {SensorsLocations.LeftKnee, 21 },
            {SensorsLocations.LeftHip, 23 },
            {SensorsLocations.Pelvic, 6 },
            {SensorsLocations.RightFoot, 16 },
            {SensorsLocations.RightKnee, 20 },
            {SensorsLocations.RightHip, 24 }
        };

        public static int GetSensorId(SensorsLocations location) {
            if (IsWirelessSensorMap) {
                if (WirelessSensorsMap.ContainsKey(location))
                    return WirelessSensorsMap[location];
                else
                    return -1;
            }
            else {
                return (WireSensorMap.ContainsKey(location)) ? WireSensorMap[location] : -1;
            }
        }

        public static int GetSensorIndex(int sensorId) {
            var temp = WireSensorMap;
            if (IsWirelessSensorMap) {
                temp = WirelessSensorsMap;
            }

            if (temp.ContainsValue(sensorId)) {
                return (int)temp.FirstOrDefault(x => x.Value == sensorId).Key - 1;
            }
            return -1;
        }
        
    }
}
