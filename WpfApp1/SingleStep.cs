﻿namespace WpfApp1 {
    public class SingleStep {
        
        public enum Leg {
            Left = 1,
            Right = 2,
            Unknown = 4
        }

        /// <summary>
        /// Czas rozpoczęcia kroku [s]
        /// </summary>
        public double StartTime { get; set; }

        /// <summary>
        /// Czas zakończenia kroku [s]
        /// </summary>
        public double? EndTime { get; set; }

        /// <summary>
        /// Iteracja rozpoczęcia kroku - odpowiednik 'rightFootInAir' i 'leftFootInAir' w inżynierce
        /// </summary>
        public int StartIteration { get; set; }

        /// <summary>
        /// Iteracja zakończenia kroku - odpowiednik 'rightFootSteps' i 'leftFootSteps' w inżynierce
        /// </summary>
        public int? EndIteration { get; set; }

        /// <summary>
        /// Długość kroku [cm] - wymaga wypełnienia danych pacjenta
        /// </summary>
        public double Length { get; set; }

        /// <summary>
        /// Której nogi dotyczy krok
        /// </summary>
        public Leg WhichLeg { get; set; }

        /// <summary>
        /// Czy krok się skończył - czy stopa została położona na podłożu
        /// </summary>
        public bool IsCompleted => !EndTime.HasValue;

        /// <summary>
        /// Metoda do wyczyszczenia 'kompletności' kroku - w sytuacji, gdy krok został błędnie zakończony.
        /// </summary>
        public void UncompleteStep() {
            Length = 0;
            EndTime = null;
            EndIteration = null;
        }

    }
}
