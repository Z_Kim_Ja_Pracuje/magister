﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    /// <summary>
    /// Pobieranie danych z app.config
    /// </summary>
    public class AppConfigManager
    {
        private static readonly string _danePacjentaPathKey = "DanePacjentaPath";
        private static readonly string _poprzednieWynikiPathKey = "PoprzednieWynikiPath";

        public static string DanePacjentaPath => System.Configuration.ConfigurationManager.AppSettings[_danePacjentaPathKey];
        public static string PoprzednieWynikiPath => System.Configuration.ConfigurationManager.AppSettings[_poprzednieWynikiPathKey];

    }
}
