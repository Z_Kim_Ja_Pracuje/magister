﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Model;

namespace WpfApp1
{
    public class CalculationManager
    {
        private static readonly ILog _log = LogManager.GetLogger("");

        private Sensor[] _sensorList;
        private Sensor[] _compareList;
        private Data data;
        private Data compareData;
        private bool fileCalculation;

        public Sensor[] SensorList {
            get { return _sensorList; }
            private set { _sensorList = value; }
        }

        private int Counter { get; set; }
        public static double ToDeg { get { return 180.0/ Math.PI; } }
        public static double ToRad { get { return 1.0/ToDeg; } }
        public static double HalfPI => Math.PI / 2.0;
        

        /// <summary>
        /// Początkowe rozbieżności pomiędzy prawidłowym przypieciem czujnika a aktualnym przypięciem.
        /// </summary>
        private V3[] StartAccAngles { get; set; }

        public enum FootState {
            InAir = 1,
            OnGround = 2
        }

        private FootState LeftFootState { get; set; }
        private FootState RightFootState { get; set; }
        public Data Data { get => data; set => data = value; }
        public Data CompareData { get => compareData; set => compareData = value; }
        public Sensor[] CompareList { get => _compareList; set => _compareList = value; }


        // public bool Calibration { get; set; }

        // tu przyda się szereg zmiennych, m.in jakiś licznik, żeby było wiadomo która iteracja

        public CalculationManager(Sensor[] sensors, Data d) {
            SensorList = sensors;
            Counter = 0;
            StartAccAngles = new V3[sensors.Length];
            Data = d;
        }

        
        /// <summary>
        /// Tu bym się nie bawił jakoś szczególnie tylko wjebać stary kod, przerobić go i elo
        /// ew. można coś pokombinować z kwaternionem ale nie wiem, czy jest sens
        /// </summary>
        public void CalculateAngles() {
            switch (GlobalVariables.SimulationType) {
                case Type.Wire:
                    break;
                case Type.Wireless:
                    if (GlobalVariables.IsCompare) {
                        CalculateAnglesFiles();
                    }
                    else {
                        CalculateAnglesWireless();
                        Counter++;
                    }
                    break;
                case Type.File:
                    CalculateAnglesFiles();
                    break;
            }
            
        }

        private void CalculateAnglesWireless() {
            CalculateForEachSensor(SensorList, Data, Counter);
        }

        /// <summary>
        /// Metoda będzie liczyć kąty na podstawie danych z plików
        /// </summary>
        private void CalculateAnglesFiles() {
            var destination = (GlobalVariables.IsCompare ? GlobalVariables.DataCompare : GlobalVariables.Data);
            var sensors = (GlobalVariables.IsCompare ? GlobalVariables.SensorManager.CompareList : GlobalVariables.SensorManager.SensorList);
            fileCalculation = true;


            for(int i = 0; i < sensors[0].Data.Count; i++) {
                // inkrementacja Counter albo jakis refactor
                Console.WriteLine($"CalculateAnglesFiles, Iteracja {i}");
                CalculateForEachSensor(sensors, destination, i);
            }
            fileCalculation = false;
            // dla bezpieczeństwa
            Counter = 0;

        }

        private void CalculateForEachSensor(Sensor[] sensorList, Data dataDest, int counter) {
            if (counter == 0) {
                // faza 1 - ustawienie kątów startowych
                // nie ma sensu tutaj liczyć z filtrem

                // tu trzeba będzie jeszcze przejrzeć, czy osie są prawidłowo
                foreach (Sensor s in sensorList) {
                    if (s.Data.Count <= counter)
                        continue;
                    V3 euler = new V3();
                    V3 acc = s.Data[counter].Acc;
                    V3 mag = s.Data[counter].Mag;
                    euler.X = Math.Atan2(acc.Y, acc.Z);
                    euler.Y = Math.Atan(acc.X / Math.Sqrt(acc.Y * acc.Y + acc.Z * acc.Z));
                    euler.Z = Math.Atan2(mag.Y, mag.X);
                    euler *= ToDeg;
                    s.Data[counter].Euler = euler;

                    //policzenie kwaternionu
                    s.CalculateQuaternion();
                }

                SetStartAngles(sensorList, dataDest);
                SetStartAccAngles(sensorList);
                // Calculations(GlobalVariables.Data, Counter, SensorList[0].ActTime);
            }
            else {
                foreach (Sensor s in sensorList) {
                    if (s.Data.Count <= counter)
                        continue;
                    V3 euler = new V3();
                    V3 acc = s.Data[counter].Acc;
                    V3 mag = s.Data[counter].Mag;
                    V3 gyro = s.Data[counter].Gyro;
                    V3 preEuler = s.Data[counter].Euler;
                    double dt = s.Data[counter].Delta;
                    double a = 0.02;
                    euler.X = Math.Atan2(acc.Y, acc.Z) * ToDeg * a + (1 - a) * (preEuler.X + dt * gyro.X);
                    euler.Y = Math.Atan(acc.X / Math.Sqrt(acc.Y * acc.Y + acc.Z * acc.Z)) * ToDeg * a + (1 - a) * (preEuler.Y + dt * gyro.Y);
                    euler.Z = Math.Atan2(mag.Y, mag.X) * ToDeg * a + (1 - a) * (preEuler.Z + dt * gyro.Z);

                    s.Data[counter].Euler = euler;

                    // policzyc kwaternion
                    s.CalculateQuaternion();
                    // Calculations(GlobalVariables.Data, Counter, SensorList[0].ActTime);
                }
                Calculations(dataDest, counter, sensorList[0].ActTime, sensorList);
            }
        }

        /// <summary>
        /// Wywołanie metod do przeliczania kątów na czujniku, ustawienia, policzenia kroków
        /// </summary>
        private void Calculations(Data data, int i, double time, Sensor[] sensorList) {
            if (fileCalculation) {
                CalcLeftSideAngles(sensorList, i);
                //  CalcRightSideAngles();
                SetAngles(data, sensorList, i);
                // checkFeetState(i, time);
            }
            else {
                CalcLeftSideAngles(sensorList);
                //  CalcRightSideAngles();
                SetAngles(data, sensorList);
            }

        }

        /// <summary>
        /// Ustawienie kątów początkowych wynikających z przypięcia czujników - OK
        /// </summary>
        private void SetStartAngles(Sensor[] sensorList, Data target) {
            // ręcznie dla każdego czujnika
            
            try {
                // stopa
                sensorList[0].Data[0].ActAngles = new V3();

                // kostka
                sensorList[1].Data[0].ActAngles = new V3(0, 90, 0);

                // Kolano
                sensorList[2].Data[0].ActAngles = new V3(0, 90, 0);

                // Miednica
                sensorList[3].Data[0].ActAngles = new V3();
                
                // warunek dla 4 czujników - jeśli są tylko 4 czujniki to licz tylko dla lewej nogi
                if (sensorList.Length > 4) {
                    // stopa
                    sensorList[4].Data[0].ActAngles = new V3();

                    // kostka
                    sensorList[5].Data[0].ActAngles = new V3(0, 90, 0);

                    // Kolano
                    sensorList[6].Data[0].ActAngles = new V3(0, 90, 0);

                    // Miednica
                    sensorList[7].Data[0].ActAngles = new V3();
                }

                SetAngles(target, sensorList, 0);
            }
            catch(Exception ex) {
                _log.Error(ex);
            }

        }

        /// <summary>
        /// Obliczenie początkowych kątów ze wskazań kwaterniona. Umożliwia korektę błędów powstałych na wskutek niewłaściwego przypięcia czujnika. - OK
        /// </summary>
        private void SetStartAccAngles(Sensor[] sensorList) {
            // liczyć z kwaterniona i mieć nadzieję, że wszystko jest ok
            // czy może liczyć z pierwszego wskazania akcelerometru?
            // wtedy g = SensorList[0].Data[0].Acc;

            // nie wiem czy jest sens liczyć kwaternion, potem wyznaczać z niego grawitację etc.
            // to i tak ma zadziałać 2-3 razy i elo, wystarczy
            // z drugiej strony możnaby się pochwalić, że jest coś więcej niż poprzednio
            // bo na obecną chwilę jest mniej
            
            V3 g;

            // trzeba będzie przejrzeć, czy osie są prawidłowo

            // lewa stopa
            // 
            try {
                g = GravityForce(sensorList[0].Data[0].Q);
                StartAccAngles[0] = new V3(Math.Atan(g.Y / (Math.Sqrt(g.X * g.X + g.Z * g.Z) * Math.Sign(g.Z))), -Math.Atan(g.X / g.Z), 0) * ToDeg;
                //new V3(Math.Atan(g.Y / (Math.Sqrt(g.X * g.X + g.Z * g.Z) * Math.Sign(g.Z))), -Math.Atan(g.X / g.Z), 0) * toDegrees;

                // lewa kostka
                //
                g = GravityForce(sensorList[1].Data[0].Q);
                StartAccAngles[1] = new V3(0, Math.Atan(g.Z / g.X), Math.Atan(g.Y / g.X)) * ToDeg;

                // lewe kolano
                //
                g = GravityForce(sensorList[2].Data[0].Q);
                StartAccAngles[2] = new V3(0, Math.Atan(g.Z / g.X), Math.Atan(g.Y / g.X)) * ToDeg;

                // miednica - lewa strona
                //
                g = GravityForce(sensorList[3].Data[0].Q);
                StartAccAngles[3] = new V3(Math.Atan(g.Z / g.Y), 0, Math.Atan(g.X / g.Y));

                // warunek dla 4 czujników - jeśli są tylko 4 czujniki to licz tylko dla lewej nogi
                if(SensorList.Length > 4) {
                    // prawa stopa
                    //
                    g = GravityForce(sensorList[4].Data[0].Q);
                    StartAccAngles[4] = new V3(Math.Atan(g.Y / (Math.Sqrt(g.X * g.X + g.Z * g.Z) * Math.Sign(g.Z))), Math.Atan(g.X / g.Z), 0) * ToDeg;

                    // prawa kostka
                    //
                    g = GravityForce(sensorList[5].Data[0].Q);
                    StartAccAngles[5] = new V3(0, -Math.Atan(g.Z / g.X), -Math.Atan(g.Y / g.X)) * ToDeg;

                    // prawe kolano
                    //
                    g = GravityForce(sensorList[6].Data[0].Q);
                    StartAccAngles[6] = new V3(0, -Math.Atan(g.Z / g.X), -Math.Atan(g.Y / g.X)) * ToDeg;

                    // miednica - prawa strona
                    //
                    g = GravityForce(sensorList[7].Data[0].Q);
                    StartAccAngles[7] = new V3(Math.Atan(g.Z / g.Y), 0, Math.Atan(g.X / g.Y));
                }
            }
            catch (Exception ex) {
                _log.Error(ex);
            }


        }

        /// <summary>
        /// Wyznaczenie kierunku wektora grawitacji
        /// </summary>
        /// <param name="q">Kwaternion</param>
        /// <returns>Kierunek grawitacji (znormalizowany)</returns>
        private V3 GravityDirection(Quat q) {
            V3 g = new V3();

           //  V3 g = new V3();
            g.Z = (q.cos * q.cos - q.x * q.x - q.y * q.y + q.z * q.z);

            double l = Math.Sqrt(1.0 - (g.Z * g.Z));
            g.X = 2 * (q.cos * q.y + q.z * q.x);

            g.Y = 2 * (q.y * q.z - q.cos * q.x);
            g.Normalize();

            return g;
        }

        /// <summary>
        /// Wyznaczenie wektora grawitacji
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public V3 GravityForce(Quat q) {
            V3 g = GravityDirection(q);

            g *= GlobalVariables.Gravity;
            return g;
        }

        /// <summary>
        /// Usunięcie grawitacji z odczytu akcelerometru
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public V3 NegateGravity(DataRow data) {
            V3 acc = data.Acc.Copy();
            V3 g = GravityForce(data.Q);
            
            acc -= g;
            
            return acc;
        }

        /// <summary>
        /// Przygotowanie danych - wektora grawitacji, żyroskopu i kątów z poprzedniej iteracji
        /// </summary>
        /// <param name="data">Dane z czujnika - aktualne</param>
        /// <param name="id">Nr czujnika - numeracja od 0</param>
        /// <param name="counter">W przypadku wczytywania z plików: nr iteracji - 1; w innym przypadku wartość domyślna</param>
        /// <returns></returns>
        private (V3 g, V3 gyro, V3 angles, double dt) PrepareData(SensorsLocations location, Sensor[] sensorList, int counter = -1) {

            //_log.Info($"PrepareData, Location: {location}, SensorList: {sensorList}, counter: {counter}" +
            //    $"\nData: {GlobalVariables.SensorManager.GetSensorValue(counter + (counter > -1 ? 1 : 0), location)}");

            var data = GlobalVariables.SensorManager.GetSensorValue(counter + (counter > -1 ? 1 : 0), location);
            int id = GlobalVariables.SensorManager.GetSensorId(location);
            V3 g = GravityForce(data.Q);

            V3 gyro = data.Gyro.Rotate(StartAccAngles[id]);

            // mozliwe, ze tu sie wysrywa
            V3 angles = ((counter == -1) ? sensorList[id].Data[sensorList[id].Data.Count - 2] : sensorList[id].GetValue(counter))?.ActAngles;

            RemoveGyroDrift(ref gyro);

            return (g: g, gyro: gyro, angles: angles, dt: data.Delta);
        }

        /// <summary>
        /// Usuwanie dryftu żyroskopu - dla każdej osi osobno.
        /// </summary>
        /// <param name="gyro">Wektor żyroskopu</param>
        private void RemoveGyroDrift(ref V3 gyro) {
            if (gyro.X < GlobalVariables.GyroDrift) gyro.X = 0;
            if (gyro.Y < GlobalVariables.GyroDrift) gyro.Y = 0;
            if (gyro.Z < GlobalVariables.GyroDrift) gyro.Z = 0;
        }

        /// <summary>
        /// trzeba ominąć 0
        /// </summary>
        /// <param name="counter"></param>
        /// <returns></returns>
        private int GetCounterForPrepareData(int counter) => (counter - (counter > 0 ? 1 : 0));

        /// <summary>
        /// Liczenie kątów dla lewej nogi.
        /// </summary>
        private void CalcLeftSideAngles(Sensor[] sensorList, int counter = -1) {

            // współczynnik filtru komplementarnego
            double a = GlobalVariables.FilterRatio;
            int calcCounter = GetCounterForPrepareData(counter);
            // lewa stopa
            //
            V3 s0A = new V3();
            (V3 g, V3 gyro, V3 angles, double dt) = PrepareData(SensorsLocations.LeftFoot, sensorList, calcCounter);

            s0A.X = (1 - a) * (angles.X + dt * gyro.X) + a * Math.Atan(g.Y / Math.Sqrt(g.X * g.X + g.Z * g.Z) * Math.Sign(g.Z)) * ToDeg;
            s0A.Y = (1 - a) * (angles.Y + dt * gyro.Y) + a * (-Math.Atan(g.X / g.Z) * ToDeg);
            s0A.Z = angles.Z + dt * gyro.Z;

            GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.LeftFoot).ActAngles = s0A;

            // lewa kostka
            //
            V3 s1A = new V3();
            (g, gyro, angles, dt) = PrepareData(SensorsLocations.LeftKnee, sensorList, calcCounter);

            s1A.Y = (1 - a) * (angles.Y + dt * gyro.Y) + a * (Math.Atan(g.Z / g.X) * ToDeg);
            s1A.Z = (1 - a) * (angles.Z - dt * gyro.Z) + a * ((Math.Atan(g.Y / g.X) + HalfPI) * ToDeg);
            s1A.X = angles.X - dt * gyro.X;

            GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.LeftKnee).ActAngles = s1A;
            // SensorList[1].Data.Last().ActAngles = s1A;

            // lewe kolano
            // 
            V3 s2A = new V3();
            (g, gyro, angles, dt) = PrepareData(SensorsLocations.LeftHip, sensorList, calcCounter);

            s2A.Y = (1 - a) * (angles.Y + dt * gyro.Y) + a * (Math.Atan(g.Z / g.X) * ToDeg);
            s2A.Z = (1 - a) * (angles.Z - dt * gyro.Z) + a * ((Math.Atan(g.Y / g.X) + HalfPI) * ToDeg);
            s2A.X = angles.X - dt * gyro.X;

            GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.LeftHip).ActAngles = s2A;
            // SensorList[2].Data.Last().ActAngles = s2A;

            // miednica - lewa strona
            //
            V3 s3A = new V3();
            (g, gyro, angles, dt) = PrepareData(SensorsLocations.Pelvic, sensorList, calcCounter);

            s3A.X = (1 - a) * (angles.X - dt * gyro.X) + a * (Math.Atan(g.Z / g.Y) * ToDeg);
            s3A.Y = angles.Y + dt * gyro.Y;
            s3A.Z = (1 - a) * (angles.Z + dt * gyro.Z) + a * (Math.Atan(g.X / g.Y) * ToDeg);

            GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.Pelvic).ActAngles = s3A;
            // SensorList[3].Data.Last().ActAngles = s3A;
        }

        /// <summary>
        /// Liczenie kątów dla prawej nogi.
        /// </summary>
        private void CalcRightSideAngles(Sensor[] sensorList, int counter = -1) {

            // współczynnik filtru komplementarnego
            double a = GlobalVariables.FilterRatio;
            int calcCounter = GetCounterForPrepareData(counter);
            // prawa stopa
            //
            V3 s4A = new V3();
            (V3 g, V3 gyro, V3 angles, double dt) = PrepareData(SensorsLocations.RightFoot, sensorList, calcCounter);

            s4A.X = (1 - a) * (angles.X + dt * gyro.X) + a * (Math.Atan(g.Y / (Math.Sqrt(g.X * g.X + g.Z * g.Z) * Math.Sign(g.Z))) * ToDeg);
            s4A.Y = (1 - a) * (angles.Y - dt * gyro.Y) + a * (Math.Atan(g.X / g.Z) * ToDeg);
            s4A.Z = angles.Z + dt * gyro.Z;

            GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.RightFoot).ActAngles = s4A;
            // SensorList[4].Data.Last().ActAngles = s4A;

            // prawa kostka
            //
            V3 s5A = new V3();
            (g, gyro, angles, dt) = PrepareData(SensorsLocations.RightKnee, sensorList, calcCounter);

            s5A.Y = (1 - a) * (angles.Y - dt * gyro.Y) + a * ((-Math.Atan(g.Z / g.X)) * ToDeg);
            s5A.Z = (1 - a) * (angles.Z + dt * gyro.Z) + a * ((-Math.Atan(g.Y / g.X) + HalfPI) * ToDeg);
            s5A.X = angles.X - dt * gyro.X;

            GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.RightFoot).ActAngles = s4A;
            // SensorList[5].Data.Last().ActAngles = s5A;

            // prawe kolano
            //
            V3 s6A = new V3();
            (g, gyro, angles, dt) = PrepareData(SensorsLocations.RightHip, sensorList, calcCounter);

            s6A.Y = (1 - a) * (angles.Y - dt * gyro.Y) + a * ((-Math.Atan(g.Z / g.X)) * ToDeg);
            s6A.Z = (1 - a) * (angles.Z + dt * gyro.Z) + a * ((-Math.Atan(g.Y / g.X) + HalfPI) * ToDeg);
            s6A.X = angles.X - dt * gyro.X;

            GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.RightFoot).ActAngles = s4A;
            // SensorList[6].Data.Last().ActAngles = s6A;

            // miednica - prawa strona
            //
            // i tak nie ma jak tego liczyć - bez sensu
            //V3 s7A = new V3();
            //(g, gyro, angles, dt) = PrepareData(SensorsLocations.RightFoot, sensorList, calcCounter);

            //s7A.X = (1 - a) * (angles.X - dt * gyro.X) + a * (Math.Atan(g.Z / g.Y) * ToDeg);
            //s7A.Y = angles.Y + dt * gyro.Y;
            //s7A.Z = (1 - a) * (angles.Z + dt * gyro.Z) + a * (Math.Atan(g.X / g.Y) * ToDeg);

            //SensorList[3].Data.Last().ActAngles = s7A;
        }
        
        /// <summary>
        /// Policzenie kątów i wstawienie ich do obiektu Data
        /// </summary>
        /// <param name="data">Klasa z listami kątów</param>
        private void SetAngles(Data data, Sensor[] sensorList, int counter = -1) {
            try {
                // kąty w kolanach
                // przerobić - jeżeli counter == -1 -> bierzemy Last(), inaczej bierzemy Data[counter]
                // PRZEROBIC RESZTE METOD
                // EHH doszło mapowanie - przerobić wszystko

                //_log.Info($"SetAngles(), counter = {counter}, " +
                //    $"\nLeftFoot: {GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.LeftFoot)}" +
                //    $"\nLeftKnee: {GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.LeftKnee)}" +
                //    $"\nLeftHip: {GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.LeftHip)}" +
                //    $"\nPelvic: {GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.Pelvic)}");



                V3 leftFootAngles = GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.LeftFoot).ActAngles,
                    leftKneeAngles = GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.LeftKnee).ActAngles,
                    leftHipAngles = GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.LeftHip).ActAngles,
                    pelvicAngles = GlobalVariables.SensorManager.GetSensorValue(counter, SensorsLocations.Pelvic).ActAngles;
                

                data.LeftKneeRotation.Add(leftKneeAngles.X);
                data.LeftKneeAngle.Add(leftKneeAngles.Z - leftHipAngles.Z);
                data.LeftKneeAdduction.Add(leftKneeAngles.Y - leftHipAngles.Y);

                if(sensorList.Length > 4) {
                    data.RightKneeRotation.Add(sensorList[5].GetValue(counter).ActAngles.X);
                    data.RightKneeAngle.Add(sensorList[5].GetValue(counter).ActAngles.Z - sensorList[6].GetValue(counter).ActAngles.Z);
                    data.RightKneeAdduction.Add(sensorList[5].GetValue(counter).ActAngles.Y - sensorList[6].GetValue(counter).ActAngles.Y);
                }

                // kąty w biodrach

                data.LeftHipRotation.Add(leftHipAngles.X);
                data.LeftHipAngle.Add(leftHipAngles.Z - pelvicAngles.Z);
                data.LeftHipAdduction.Add(leftHipAngles.Y); // możliwe że trzeba będzie odnieść się do miednicy

                if(sensorList.Length > 4) {
                    data.RightHipRotation.Add(sensorList[6].GetValue(counter).ActAngles.X);
                    data.RightKneeAngle.Add(sensorList[6].GetValue(counter).ActAngles.Z - sensorList[7].GetValue(counter).ActAngles.Z);
                    data.RightHipAdduction.Add(sensorList[6].GetValue(counter).ActAngles.Y);
                }

                // kąty miednicy

                if(sensorList.Length > 4) {
                    data.PelvicRotation.Add((sensorList[3].GetValue(counter).ActAngles.Y + sensorList[7].GetValue(counter).ActAngles.Y) / 2.0);
                    data.PelvicAngle.Add((sensorList[3].GetValue(counter).ActAngles.X + sensorList[7].GetValue(counter).ActAngles.X) / 2.0);
                    data.PelvicAdduction.Add((sensorList[3].GetValue(counter).ActAngles.Z + sensorList[7].GetValue(counter).ActAngles.Z) / 2.0);
                }
                else {
                    data.PelvicRotation.Add(pelvicAngles.Y);
                    data.PelvicAngle.Add(pelvicAngles.X );
                    data.PelvicAdduction.Add(pelvicAngles.Z);
                }

                // kąty w kostce

                data.LeftFeetRotation.Add(leftFootAngles.Z); // mozliwe odniesienie do kolana
                data.LeftFeetAngle.Add(leftKneeAngles.Z - leftFootAngles.Y);
                data.LeftFeetAdduction.Add(leftFootAngles.X); // możliwe że odniesienie do kolana

                if(sensorList.Length > 4) {
                    data.RightFeetRotation.Add(sensorList[4].GetValue(counter).ActAngles.Z);
                    data.RightFeetAngle.Add(sensorList[5].GetValue(counter).ActAngles.Z - sensorList[4].GetValue(counter).ActAngles.Y);
                    data.RightFeetAdduction.Add(sensorList[4].GetValue(counter).ActAngles.X);
                }
            }
            catch (Exception ex) {
                _log.Error(ex);
            }
           
        }

        /// <summary>
        /// Funkcja sprawdza czy pacjent jest w fazie przenoszenia (jedna stopa na podłożu, druga się porusza), 
        /// czy w fazie podporu pełnego (obie stopy na podłożu).
        /// Pasuje to przerobić stricte na czas - tylko jak?
        /// </summary>
        private void CheckMovePhase() {
            //if (leftFoot == FeetState.OnGround && rightFoot == FeetState.OnGround) {
            //    bothFeetsOnGround.Add(1);
            //    oneFootInAir.Add(0);
            //}
            //else if (leftFoot == FeetState.OnGround && rightFoot == FeetState.InAir) {
            //    oneFootInAir.Add(2);
            //    bothFeetsOnGround.Add(0);
            //}
            //else if (leftFoot == FeetState.InAir && rightFoot == FeetState.OnGround) {
            //    oneFootInAir.Add(1);
            //    bothFeetsOnGround.Add(0);
            //}
            //else {
            //    // pacjent podskakuje albo zaczął latać
            //    oneFootInAir.Add(0);
            //    bothFeetsOnGround.Add(0);
            //}
        }

        /// <summary>
        /// Funkcja oblicza długość kroku podaną nogą
        /// Powinno działać - trzeba sprawdzić mapowania
        /// </summary>
        /// <param name="i">1 - prawa noga; 0 - lewa noga</param>
        private void CalcStepLength(int i, Sensor[] sensorList, Data data) {
            /*
             // raczej źle - nie uwzględnia, że pięta drugiej stopy może się oderwać od podłoża
             // miednica się wtedy przesuwa i jest błąd rzędu 10-20cm
             // ale tu potrzeba testów
            */
            double stepLength = 0.0;
            Pacjent pd = GlobalVariables.PatientData;
            // if (pd == null || !pd.isFilled) return; // jeśli nie ma danych długości nóg pacjenta to nie ma sensu liczyć długości kroków
            /*
             * rk - rozpoczęcie kroku
             * zk - zakończenie kroku
             * 
             sin1 - sinus zmiany kąta pochylenia kości udowej [nogi, którą jest wykonany krok] względem podłoża między rk a zk
             cos1 - cosinus kąta pochylenia strzałki [nogi, którą jest wykonany krok] w chwili zk
             cos2 - cosinus kąta pochylenia strzałki [nogi, którą jest wykonany krok] w chwili rk

             sin2 - sinus zmiany kąta pochylenia strzałki [nogi, która stoi na podłożu] między rk i zk
             sin3 - sinus zmiany kąta pochylenia kości udowej [nogi, która stoi na podłożu] w chwili rk
             sin4 - sinus zmiany kąta pochylenia kości udowej [nogi, która stoi na podłożu] w chwili zk

             sin5 - sinus kąta rotacji miednicy w chwili rk;
             sin6 - sinus kąta rotacji miednicy w chwili zk;

             generalnie liczenie długości kroku to:
                - policzenie przemieszczenia miednicy
                - policzenia odległości wychylenia miednicy
                - policzenie przemieszczenia stopy [nogi, którą jest wykonany krok]
             
             */

            // I - dane pacjenta, raczej w GlobalVariables
            // II - aktualny krok, chyba ten, który jest ostatni na liście kroków?

            // trzeba gdzieś te kroki poumieszczać - gdzie? które data?

            if (i == 0) { // krok lewą nogą

                // chyba nie trzeba szukać ostatniego kompletnego - metoda odpala się, kiedy krok jest zakończony
                SingleStep step = Data.LeftLegSteps.Last();

                int start = step.StartIteration;
                int end = step.EndIteration.Value; // bez sprawdzenia - wartość powinna być

                // kierwa, który to s8? trzeba sprawdzić w domu
                // roboczo zakładam, że to ten na kolanie - ale nie wiem
                double a1 = sensorList[2].Data[start].ActAngles.Z * ToRad; // s[8].data[start].Angles.Z * toRadian;
                double a2 = sensorList[2].Data[end].ActAngles.Z * ToRad; // s[8].data[end].Angles.Z * toRadian;

                double g1 = (180 - Data.RightKneeAngle[end]) * ToRad; // (180 - rightKneeAngle[end]) * toRadian;
                double g2 = (180 - Data.RightKneeAngle[start]) * ToRad; // (180 - rightKneeAngle[start]) * toRadian;

                // jak to było mapowane? sprawdzić w domu bo zrobione na czuja
                // double l1 = pd.left[0], l2 = pd.left[1], l3 = pd.right[0], l4 = pd.right[1];
                double l1 = pd.LewaStrzalka, l2 = pd.LeweUdo, l3 = pd.PrawaStrzalka, l4 = pd.PraweUdo;

                double x1 = Math.Sqrt(l3 * l3 + l4 * l4 - 2 * l3 * l4 * Math.Cos(g1));
                double g3 = Math.Acos((l4 * l4 - x1 * x1 - l3 * l3) / (-2 * l3 * x1));

                double x2 = Math.Sqrt(l3 * l3 + l4 * l4 - 2 * l3 * l4 * Math.Cos(g2));
                double g4 = Math.Acos((l4 * l4 - x2 * x2 - l3 * l3) / (-2 * l3 * x2));

                double g5 = a1 + g4 - a2 - g3;

                double x5 = Math.Sqrt(x1 * x1 + x2 * x2 - 2 * x1 * x2 * Math.Cos(g5));
                // double a3 = Math.Abs(s[6].data[end].Angles.Y - s[6].data[start].Angles.Y) * toRadian;
                //double x4 = x5 + 2 * pd.pelvicLength * pd.pelvicLength * (1 - Math.Cos(a3));

                double x4 = x5;
                // przemieszczenie miednicy policzone

                double b1 = (180 - Data.LeftKneeAngle[end]) * ToRad; // (180 - leftKneeAngle[end]) * toRadian;
                double b2 = (180 - Data.LeftKneeAngle[start]) * ToRad; // (180 - leftKneeAngle[start]) * toRadian;

                double x11 = Math.Sqrt(l1 * l1 + l2 * l2 - 2 * l1 * l2 * Math.Cos(b1));
                double g11 = Math.Acos((l2 * l2 - l1 * l1 - x11 * x11) / (-2 * l1 * x11));

                double x22 = Math.Sqrt(l1 * l1 + l2 * l2 - 2 * l1 * l2 * Math.Cos(b2));
                double g22 = Math.Acos((l2 * l2 - l1 * l1 - x22 * x22) / (-2 * l1 * x22));

                // który to s7? sprawdzić
                double a4 = (180 - sensorList[6].Data[end].ActAngles.Z - g11 * ToDeg) * ToRad; // (180 - s[7].data[end].Angles.Z - g11 * toDegrees) * toRadian;
                double a5 = (sensorList[6].Data[start].ActAngles.Z + g22 * ToDeg) * ToRad; // (s[7].data[start].Angles.Z + g22 * toDegrees) * toRadian;

                stepLength = x4 + x11 * Math.Cos(a4) + x22 * Math.Cos(a5);
                step.Length = stepLength;

                // na razie komentuję - z 4 czujnikami i tak gówno się da policzyć
                // data.LeftLegSteps.Add(step);
                // leftFootStepsLength.Add(stepLength);
            }

            else if (i == 1) {

                SingleStep step = Data.RightLegSteps.Last();

                int start = step.StartIteration; // rightFootInAir[rightFootInAir.Count - 1];
                int end = step.EndIteration.Value; // rightFootSteps[rightFootSteps.Count - 1];

                // robocze założenie że s7 odpowiada obecnemu s6
                double a1 = sensorList[6].Data[start].ActAngles.Z * ToRad; // s[7].data[start].Angles.Z * toRadian;
                double a2 = sensorList[6].Data[end].ActAngles.Z * ToRad; // s[7].data[end].Angles.Z * toRadian;

                double g1 = (180 - Data.LeftKneeAngle[end]) * ToRad; // (180 - leftKneeAngle[end]) * toRadian;
                double g2 = (180 - Data.LeftKneeAngle[start]) * ToRad; // (180 - leftKneeAngle[start]) * toRadian;

                // trzeba sprawdzić mapowania
                //double l3 = pd.left[0], l4 = pd.left[1], l1 = pd.right[0], l2 = pd.right[1];
                double l3 = pd.LewaStrzalka, l4 = pd.LeweUdo, l1 = pd.PrawaStrzalka, l2 = pd.PraweUdo;

                double x1 = Math.Sqrt(l3 * l3 + l4 * l4 - 2 * l3 * l4 * Math.Cos(g1));
                double g3 = Math.Acos((l4 * l4 - x1 * x1 - l3 * l3) / (-2 * l3 * x1));

                double x2 = Math.Sqrt(l3 * l3 + l4 * l4 - 2 * l3 * l4 * Math.Cos(g2));
                double g4 = Math.Acos((l4 * l4 - x2 * x2 - l3 * l3) / (-2 * l3 * x2));

                double g5 = a1 + g4 - a2 - g3;

                double x5 = Math.Sqrt(x1 * x1 + x2 * x2 - 2 * x1 * x2 * Math.Cos(g5));

                // z tego co pamiętam s6 to miednica ale trzeba to sprawdzić
                //double a3 = Math.Abs(s[6].data[end].Angles.Y - s[6].data[start].Angles.Y) * toRadian;
                double a3 = Math.Abs(sensorList[7].Data[end].ActAngles.Y - sensorList[7].Data[start].ActAngles.Y) * ToRad;

                // double x4 = x5 + 2 * pd.pelvicLength * pd.pelvicLength * (1 - Math.Cos(a3));
                double x4 = x5 + 2 * pd.Miednica * pd.Miednica * (1 - Math.Cos(a3));

                x4 = x5;
                // przemieszczenie miednicy policzone

                double b1 = (180 - Data.RightKneeAngle[end]) * ToRad; // (180 - rightKneeAngle[end]) * toRadian;
                double b2 = (180 - Data.RightKneeAngle[end]) * ToRad; // (180 - rightKneeAngle[start]) * toRadian;

                double x11 = Math.Sqrt(l1 * l1 + l2 * l2 - 2 * l1 * l2 * Math.Cos(b1));
                double g11 = Math.Acos((l2 * l2 - l1 * l1 - x11 * x11) / (-2 * l1 * x11));

                double x22 = Math.Sqrt(l1 * l1 + l2 * l2 - 2 * l1 * l2 * Math.Cos(b2));
                double g22 = Math.Acos((l2 * l2 - l1 * l1 - x22 * x22) / (-2 * l1 * x22));

                // mapowania czujników s8
                double a4 = (180 - sensorList[2].Data[end].ActAngles.Z - g11 * ToDeg) * ToRad; // (180 - s[8].data[end].Angles.Z - g11 * toDegrees) * toRadian;
                double a5 = (sensorList[2].Data[start].ActAngles.Z + g22 * ToDeg) * ToRad; // (s[8].data[start].Angles.Z + g22 * toDegrees) * toRadian;

                stepLength = x4 + x11 * Math.Cos(a4) + x22 * Math.Cos(a5);
                step.Length = stepLength;
               //  rightFootStepsLength.Add(stepLength);
            }
        }

        // leftFootInAir - lista momentów, w których lewa noga została podniesiona do góry
        // leftFootSteps - lista momentów, w których lewa noga została postawiona na podłożu
        // leftFootStepsLength - lista długości kolejnych kroków lewą nogą
        // rightFootInAir, rightFootSteps, rightFootStepsLength - listy dla prawej nogi, analogicznie jak dla lewej
        // V3 - wektor o 3 współrzędnych: X, Y, Z

        /// <summary>
        /// Funkcja sprawdza czy stopy są na podłożu, czy się poruszają
        /// DOSTOSOWANE DO NOWEGO PODEJŚCIA - PRZETESTOWAĆ
        /// Z 4 CZUJNIKAMI I TAK GÓWNO POLICZĘ, WIĘC NIE WIEM CZY JEST SENS
        /// </summary>
        /// <param name="i">Iteracja</param>
        /// <param name="time">Aktualny czas - z ostatniego rekordu w SensorList[].Data</param>
        private void CheckFeetState(int i, double time, Sensor[] sensorList, Data data) {
            int iterationCount = GlobalVariables.IterationCount;
            if (i < iterationCount) return; // jeżeli jest mniej niż 'IterationCount' iteracji nie ma sensu sprawdzać
            
            double accTick = GlobalVariables.AccTick;
            double timeToWait = GlobalVariables.TimeToWait;
            double actTime = time; 
            

            V3 s0Acc = NegateGravity(SensorList[0].Data.Last()); // lewa noga - stopa
            V3 s0PreAcc = NegateGravity(SensorList[0].Data[SensorList[0].Data.Count - 2]);

            V3 s1Acc = NegateGravity(SensorList[1].Data.Last()); // lewa noga - kostka
            V3 s1PreAcc = NegateGravity(SensorList[1].Data[SensorList[0].Data.Count - 2]);

            V3 s4Acc = NegateGravity(SensorList[4].Data.Last()); // prawa noga - stopa
            V3 s4PreAcc = NegateGravity(SensorList[4].Data[SensorList[0].Data.Count - 2]);

            V3 s5Acc = NegateGravity(SensorList[5].Data.Last()); // prawa noga - kostka
            V3 s5PreAcc = NegateGravity(SensorList[5].Data[SensorList[0].Data.Count - 2]);

            var lastLeftStep = Data.LeftLegSteps.Count > 0 ? Data.LeftLegSteps.Last() : null; // ostatni krok lewą nogą
            var lastRightStep = Data.RightLegSteps.Count > 0 ? Data.RightLegSteps.Last() : null; // ostatni krok prawą nogą

            // jeżeli jest krok lewą nogą i krok lewą nogą nie jest zakończony i upłynął czas oczekiwania
            if (lastLeftStep != null && !lastLeftStep.IsCompleted && lastLeftStep.StartTime <= actTime - timeToWait) {
                if (LeftFootState == FootState.InAir) { // jeżeli lewa stopa jest w powietrzu

                    // liczenie wartości sił działających na czujnik (bez grawitacji) w aktualnej i poprzedniej iteracji
                    // s0Acc, s0PreAcc - wartości akcelerometru dla czujnika na stopie
                    // s1Acc, s1PreAcc - wartości akcelerometru na kostce
                    
                    // sprawdzenie, czy siły różnią się znacząco od tych w poprzedniej iteracji
                    // jeżeli tak to prawdopodobnie pięta lewej stopy uderzyła o podłoże

                    // będzie śmiesznie, jeśli ktoś będzie trafiał w okresy pomiędzy pomiarami - trzeba to uwzględnić przy testach
                    // i w książce, może nawet odnieść się do inżynierki

                    if(Math.Abs(s0Acc.X - s0PreAcc.X) + Math.Abs(s0Acc.Z - s0PreAcc.Z) + Math.Abs(s1Acc.Length - s1PreAcc.Length) > 4 * accTick ||
                       Math.Abs(s0Acc.X - s0PreAcc.X) + Math.Abs(s0Acc.Z - s0PreAcc.Z) > 3* accTick) {
                        LeftFootState = FootState.OnGround;
                        lastLeftStep.EndTime = actTime;
                        lastLeftStep.EndIteration = i;

                        Console.WriteLine($"Lewa stopa na ziemi: {actTime:F2}s; iteracja {i}");
                        _log.Info($"Lewa stopa na ziemi: {actTime:F2}s; iteracja {i}");
                        CalcStepLength(0, sensorList, data);
                        return; // wyjście, żeby nie szło dalej - coś mogłoby się zjebać
                    }
                }
            }

            // jeżeli nie było kroku lewą nogą lub jest zakończony krok lewą nogą i krok zakończył się jakiś czas temu
            if(lastLeftStep == null || (lastLeftStep.IsCompleted && lastLeftStep.EndTime.Value <= actTime - timeToWait)) { 
                if (LeftFootState == FootState.OnGround) { // jeżeli lewa stopa jest na podłozu
                    double sum = 0.0, sum2 = 0.0;
                    // obliczamy średnią kątów na kolanie(s[2]) i stopie(s[0]) z 3 poprzednich iteracji - więcej nie ma sensu
                    // nawet skłaniałbym się do rezygnacji ze średnich tylko bezpośrednio z poprzedniej iteracji

                    for (int j = 2; j < 2 + iterationCount; j++) {
                        sum += SensorList[0].Data[SensorList[0].Data.Count - j].ActAngles.Y;
                        sum2 += SensorList[2].Data[SensorList[2].Data.Count - j].ActAngles.Z;
                    }

                    sum /= iterationCount;
                    sum2 /= iterationCount;

                    // jeżeli uzyskany kąt dość znacznie różni się od średniej - pytanie, ile to jest dość znacznie? Przedtem to było odpowiednio 2 i 1 stopień na średnią
                    // teraz zmieniłem na 10 i 7 stopni na średnią, trzeba przetestować
                    if (SensorList[2].ActAngles.Z - sum2 > 10 && Math.Abs(SensorList[0].ActAngles.Y - sum2) > 7) { 

                        if((lastLeftStep?.IsCompleted ?? false) && (lastRightStep?.IsCompleted ?? false) && 
                            lastLeftStep?.EndTime > lastRightStep?.EndTime && RightFootState == FootState.OnGround) {
                            // if (leftFootSteps.Count > 0 && rightFootSteps.Count > 0 &&
                            //     leftFootSteps[leftFootSteps.Count - 1] > rightFootSteps[rightFootSteps.Count - 1] && rightFoot == FeetState.OnGround) {
                            // if do obsługi częstego błędu - sprawdza, czy przypadkiem nie ma 2 kroków lewą nogą pod rząd

                            lastLeftStep.UncompleteStep();
                            Console.WriteLine($"Pomyłka, lewa stopa w powietrzu : {actTime:F2}s; iteracja {i}");
                            _log.Info($"Pomyłka, lewa stopa w powietrzu : {actTime:F2}s; iteracja {i}");
                            LeftFootState = FootState.InAir;

                            // leftFootSteps.RemoveAt(leftFootSteps.Count - 1); // usuwamy ostatni element z listy
                            // Console.WriteLine("Pomyłka, lewa stopa w powietrzu " + i); // wypisujemy info o błędzie
                            // if (leftFootStepsLength.Count > 0)
                            //     leftFootStepsLength.RemoveAt(leftFootStepsLength.Count - 1); // usuwamy ostatni element z listy
                            // leftFoot = FeetState.InAir; // ustawiamy flagę, że lewa stopa jest w powietrzu
                            return; // no i wychodzimy
                        }

                        LeftFootState = FootState.InAir; // ustawiamy flagę, że lewa stopa w powietrzu

                        // w pętli sprawdzamy kiedy nastąpił początek ruchu
                        int it = 0;
                        double max = 0.0;
                        int endIt = SensorList[0].Data.Count - 1;
                        for (int j = endIt - iterationCount; j < endIt; j++) {
                            if(SensorList[2].Data[j].ActAngles.Z - SensorList[2].Data[j-1].ActAngles.Z > max) {
                                max = SensorList[2].Data[j].ActAngles.Z - SensorList[2].Data[j - 1].ActAngles.Z;
                                it = j;
                            }
                        }

                        // tworzymy nowy obiekt SingleStep
                        var newLastLeftStep = new SingleStep() {
                            StartTime = actTime,
                            WhichLeg = SingleStep.Leg.Left,
                            StartIteration = it
                        };

                        //for (int j = i - 9; j < i; j++) {
                        //    if (s[23].data[j].Angles.Z - s[23].data[j - 1].Angles.Z > max) {
                        //        max = s[23].data[j + 1].Angles.Z - s[23].data[j].Angles.Z;
                        //        it = j;
                        //    }
                        //}
                        //leftFootInAir.Add(it); // dodajemy aktualny czas do listy z momentami uniesienia lewej nogi

                        // if do sprawdzania jeszcze częstszego błędu
                        // błąd występuje u osób ze skłonnościami do niedojadania i anoreksji
                        // takie osoby "oszukujo" akcelerometr - biedny czujnik nie wykrywa położenia stopy na podłożu
                        // chociaż to w sumie bardziej kwestia tego, że 100 pomiarów na sekundę to w c*uj za mało
                        // przydałoby się chociaż 500 pomiarów

                        // w obecnej formie wystarczy sprawdzić tylko czy IsCompleted
                        // chociaż w sumie do takiej sytuacji nie powinno dojść
                        if (!lastRightStep?.IsCompleted ?? false) {

                            // if (rightFootInAir.Count > rightFootSteps.Count) {
                            // jeżeli prawa stopa była częściej unoszona do góry niż kładziona na ziemi
                            // tj. nie złapało w całości ostatniego kroku prawą nogą

                            int i1 = lastRightStep.StartIteration, j = 0;

                            // int i1 = rightFootInAir[rightFootInAir.Count - 1], j = 0; ;
                            // przeglądamy jeszcze raz cały przedział od momentu uniesienia prawej stopy do uniesienia lewej stopy
                            // w poszukiwaniu postawienia na podłożu stopy prawej
                            for(j = it; j> i1; j--) {
                                // for (j = leftFootInAir[leftFootInAir.Count - 1]; j > i1; j--) {

                                //odzczyty wartości sił działających na czujnik (bez grawitacji)
                                // po kolei: stopa, kostka
                                //V3 acc = negateGravity(10, j), preAcc = negateGravity(10, j - 1);
                                //V3 s16acc = negateGravity(16, j), s16preAcc = negateGravity(16, j - 1);
                                //V3 s18acc = negateGravity(18, j), s18preAcc = negateGravity(18, j - 1);

                                V3 acc = NegateGravity(SensorList[4].Data[j]), preAcc = NegateGravity(SensorList[4].Data[j - 1]);
                                V3 acc1 = NegateGravity(SensorList[5].Data[j]), preAcc1 = NegateGravity(SensorList[5].Data[j - 1]);

                                // sprawdzenie, czy uderzenie pięty o podłoże zostało zarejestrowane, tylko miało mniejszą siłę od zakładanej
                                
                                if (Math.Abs(acc.X - preAcc.X) + Math.Abs(acc.Z - preAcc.Z) + Math.Abs(acc1.Z - preAcc1.Z) + Math.Abs(acc1.X - preAcc1.X) > 3 * accTick ||
                                    Math.Abs(acc.X - preAcc.X) + Math.Abs(acc.Z - preAcc.Z) > 1.5 * accTick ||
                                    Math.Abs(acc1.Z - preAcc1.Z) + Math.Abs(acc1.X - preAcc1.X) > 1.5 * accTick) {

                                    RightFootState = FootState.OnGround; // ustawienie flagi, że prawa stopa na podłożu
                                    lastRightStep.EndIteration = j;
                                    lastRightStep.EndTime = SensorList[4].Data[j].ActTime;
                                    
                                    Console.WriteLine($"Prawa stopa na ziemi(1) : {actTime:F2}s; iteracja {i}; iteracja kroku {j}"); // wypisanie info
                                    _log.Info($"Prawa stopa na ziemi(1) : {actTime:F2}s; iteracja {i}; iteracja kroku {j}");

                                    CalcStepLength(1, sensorList, data); // policzenie długości kroku
                                    break;
                                }
                            }
                            if (j == i1) { // jeżeli nic nie znaleziono
                                // coś trzeba zrobić
                                // chyba korelację - o ile to nie będzie pierwszy krok
                                // jeżeli to pierwszy krok i nie było żadnych lewą nogą to można to po prostu wyrzucić
                                // albo policzyć jakoś na końcu - potrzeba jakiejś flagi
                            }

                        }

                        Data.LeftLegSteps.Add(newLastLeftStep); // dodanie kroku do listy kroków

                        Console.WriteLine($"Lewa stopa w powietrzu : {actTime:F2}s; iteracja {i} "); // wyświetlenie info
                        _log.Info($"Lewa stopa w powietrzu : {actTime:F2}s; iteracja {i} ");
                        return; // no i koniec funkcji
                    }
                }
            }

            // jeżeli jest krok prawą nogą i krok lewą prawą nie jest zakończony i upłynął czas oczekiwania
            if (lastRightStep != null && !lastRightStep.IsCompleted && lastRightStep.StartTime <= actTime - timeToWait) {
                if (RightFootState == FootState.InAir) { // jeżeli prawa stopa jest w powietrzu
                    
                    // liczenie wartości sił działających na czujnik(bez grawitacji) w aktualnej i poprzedniej iteracji
                    // s4Acc, s4PreAcc - wartości akcelerometru dla czujnika na stopie
                    // s5Acc, s5PreAcc - wartości akcelerometru na kostce

                    // będzie śmiesznie, jeśli ktoś będzie trafiał w okresy pomiędzy pomiarami - trzeba to uwzględnić przy testach
                    // i w książce, może nawet odnieść się do inżynierki

                    // sprawdzenie, czy siły różnią się znacząco od tych w poprzedniej iteracji
                    // jeżeli tak to prawdopodobnie pięta prawej stopy uderzyła o podłoże 

                    if (Math.Abs(s4Acc.X - s4PreAcc.X) + Math.Abs(s4Acc.Z - s4PreAcc.Z) + Math.Abs(s5Acc.Length - s5PreAcc.Length) > 4 * accTick ||
                       Math.Abs(s4Acc.X - s4PreAcc.X) + Math.Abs(s4Acc.Z - s4PreAcc.Z) > 3 * accTick) {

                        //if (Math.Abs(acc.X - preAcc.X) + Math.Abs(acc.Z - preAcc.Z) + Math.Abs(s16acc.Z - s16preAcc.Z) + Math.Abs(s16acc.X - s16preAcc.X) + Math.Abs(s18acc.Z - s18preAcc.Z) > 5 * accTick ||
                        //Math.Abs(acc.X - preAcc.X) + Math.Abs(acc.Z - preAcc.Z) > 3 * accTick ||
                        //Math.Abs(s16acc.Z - s16preAcc.Z) + Math.Abs(s16acc.X - s16preAcc.X) > 3 * accTick ||
                        //Math.Abs(s18acc.Z - s18preAcc.Z) > 3 * accTick) {
                        RightFootState = FootState.OnGround; // jeżeli siły znacznie się różnią zmieniamy flagę prawej stopy - od teraz jest na ziemi
                        lastRightStep.EndTime = actTime; // do listy z krokami dopisujemy moment postawienia stopy
                        lastRightStep.EndIteration = i;
                        
                        Console.WriteLine($"Prawa stopa na ziemi : {actTime:F2}s; iteracja {i}"); // wypisujemy info
                        _log.Info($"Prawa stopa na ziemi : {actTime:F2}s; iteracja {i}");

                        CalcStepLength(1, sensorList, data); // obliczamy długość zrobionego kroku
                        return; // no i koniec
                    }

                }
            }

            // jeżeli nie było kroku prawą nogą lub jest zakończony krok prawą nogą i krok zakończył się jakiś czas temu
            if (lastRightStep == null || (lastRightStep.IsCompleted && lastRightStep.EndTime.Value <= actTime - timeToWait)) {
                if (RightFootState == FootState.OnGround) { // jeżeli lewa stopa jest na podłozu
                //    if (rightFootSteps.Count == 0 || i - rightFootSteps[rightFootSteps.Count - 1] > ticksToWait) {
                //if (rightFoot == FeetState.OnGround) { // jeżeli prawa stopa jest na podłożu
                    double sum = 0, max = 0, sum2 = 0.0;
                    int it = 0;

                    // obliczamy średnią kątów na kolanie(s[6]) i stopie(s[7]) z 3 poprzednich iteracji - więcej nie ma sensu
                    // nawet skłaniałbym się do rezygnacji ze średnich tylko bezpośrednio z poprzedniej iteracji
                    for (int j = 2; j < 2 + iterationCount; j++) {
                        sum += SensorList[4].Data[SensorList[4].Data.Count - j].ActAngles.Y;
                        sum2 += SensorList[6].Data[SensorList[6].Data.Count - j].ActAngles.Z;
                    }

                    sum /= iterationCount;
                    sum2 /= iterationCount;

                    // jeżeli uzyskany kąt dość znacznie różni się od średniej - pytanie, ile to jest dość znacznie? Przedtem to było odpowiednio 2 i 1 stopień na średnią
                    // teraz zmieniłem na 10 i 7 stopni na średnią, trzeba przetestować
                    if (SensorList[6].ActAngles.Z - sum2 > 10 && Math.Abs(SensorList[4].ActAngles.Y - sum2) > 7) {
                        // if (s[22].data[i].Angles.Z - sum > 2 && Math.Abs(s[16].data[i].Angles.Y - sum2) > 1) { // jeżeli aktualne kąty znacznie różnią się od policzonych średnich 

                        //if do obsługi częstego błędu, czy przypadkiem nie ma 2 kroków prawą nogą pod rząd
                        if ((lastRightStep?.IsCompleted ?? false) && (lastLeftStep?.IsCompleted ?? false) &&
                            lastRightStep?.EndTime > lastLeftStep?.EndTime && LeftFootState == FootState.OnGround) {
                            //if (rightFootSteps.Count > 0 && leftFootSteps.Count > 0 &&
                            //rightFootSteps[rightFootSteps.Count - 1] > leftFootSteps[leftFootSteps.Count - 1] && leftFoot == FeetState.OnGround) {

                            lastRightStep.UncompleteStep(); // krok nie jest kompletny
                            Console.WriteLine($"Pomyłka, prawa stopa w powietrzu : {actTime:F2}s; iteracja {i}"); // info o pomyłce
                            _log.Info($"Pomyłka, prawa stopa w powietrzu : {actTime:F2}s; iteracja {i}");
                            RightFootState = FootState.InAir; // zmieniamy flagę, teraz prawa stopa jest w powietrzu

                            //rightFootSteps.RemoveAt(rightFootSteps.Count - 1); // usuwamy ostatnio dodaną wartość z listy
                            //if (rightFootStepsLength.Count > 0)
                            //    rightFootStepsLength.RemoveAt(rightFootStepsLength.Count - 1); // usuwamy ostatnio dodaną wartość z listy
                            //Console.WriteLine("Pomyłka, prawa stopa jednak w powietrzu " + i); // wyświetlamy info o pomyłce
                            //rightFoot = FeetState.InAir; // zmieniamy flagę, teraz prawa stopa jest w powietrzu
                            return; // koniec funkcji
                        }

                        RightFootState = FootState.InAir; // zmieniamy flagę, teraz prawa stopa jest w powietrzu

                        // w pętli sprawdzamy kiedy nastąpił początek ruchu 
                        int endIt = SensorList[0].Data.Count - 1;
                        for (int j = endIt - iterationCount; j < endIt; j++) {
                            if (SensorList[6].Data[j + 1].ActAngles.Z - SensorList[6].Data[j].ActAngles.Z > max) {
                                max = SensorList[6].Data[j + 1].ActAngles.Z - SensorList[6].Data[j].ActAngles.Z;
                                it = j;
                            }
                        }
                        // tworzymy nowy obiekt SingleStep
                        var newLastRightStep = new SingleStep() {
                            StartTime = actTime,
                            WhichLeg = SingleStep.Leg.Right,
                            StartIteration = it
                        };

                        // data.RightLegSteps.Add()

                        // rightFootInAir.Add(it); // dodajemy odszukaną wartość do listy z momentami podniesienia prawej stopy

                        // if do sprawdzania jeszcze częstszego błędu
                        // błąd występuje u osób ze skłonnościami do niedojadania i anoreksji
                        // takie osoby "oszukujo" akcelerometr - biedny czujnik nie wykrywa położenia stopy na podłożu
                        if (!lastLeftStep?.IsCompleted ?? false) { 
                            // if (leftFootInAir.Count > leftFootSteps.Count) {
                            // jeżeli lewa stopa była częściej unoszona do góry niż kładziona na podłożu
                            // tj nie wykryło położenia stopy
                            int i1 = lastLeftStep.StartIteration, j = 0;
                            // int i1 = leftFootInAir[leftFootInAir.Count - 1], j = 0;

                            // przeglądamy jeszcze raz cały przedział od momentu uniesienia lewej stopy do uniesienia prawej stopy
                            // w poszukiwaniu postawienia na podłożu stopy lewej
                            for (j = it; j > i1; j--) {

                                //odzczyty wartości sił działających na czujnik (bez grawitacji)
                                // po kolei: stopa, kostka
                                V3 acc = NegateGravity(SensorList[0].Data[j]), preAcc = NegateGravity(SensorList[0].Data[j - 1]);
                                V3 acc1 = NegateGravity(SensorList[1].Data[j]), preAcc1 = NegateGravity(SensorList[1].Data[j - 1]);

                                // sprawdzenie, czy uderzenie pięty o podłoże zostało zarejestrowane, tylko miało mniejszą siłę od zakładanej

                                if (Math.Abs(acc.X - preAcc.X) + Math.Abs(acc.Z - preAcc.Z) + Math.Abs(acc1.Z - preAcc1.Z) + Math.Abs(acc1.X - preAcc1.X) > 3 * accTick ||
                                    Math.Abs(acc.X - preAcc.X) + Math.Abs(acc.Z - preAcc.Z) > 1.5 * accTick ||
                                    Math.Abs(acc1.Z - preAcc1.Z) + Math.Abs(acc1.X - preAcc1.X) > 1.5 * accTick) {

                                    LeftFootState = FootState.OnGround; // zmiana flagi, lewa stopa jest teraz na podłożu
                                    lastLeftStep.EndIteration = j;
                                    lastLeftStep.EndTime = SensorList[0].Data[j].ActTime;

                                    Console.WriteLine($"Lewa stopa na ziemi : {actTime:F2}s; iteracja {j} "); // wyświtlenie info
                                    _log.Info($"Lewa stopa na ziemi : {actTime:F2}s; iteracja {j} ");
                                    CalcStepLength(0, sensorList, data); // policzenie długości kroku
                                    break;
                                }
                            }
                            if (j == i1) { // jeżeli nic nie znaleziono
                                // jakaś obsługa błędu
                                // albo korelacja
                                // chyba, że pierwszy krok, wtedy wyczyścić i yolo
                            }
                        }

                        Data.RightLegSteps.Add(newLastRightStep); // dodanie kroku do listy kroków

                        Console.WriteLine($"Prawa stopa w powietrzu : {actTime:F2}s; iteracja {it}"); // wypisanie info
                        _log.Info($"Prawa stopa w powietrzu : {actTime:F2}s; iteracja {it}");
                        
                        return; // koniec funkcji
                    }
                }
            }

        }

    }
}
