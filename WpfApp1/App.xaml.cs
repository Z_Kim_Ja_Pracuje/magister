﻿using System.Windows;
using log4net;

namespace WpfApp1 {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override void OnStartup(StartupEventArgs e) {
            log4net.Config.XmlConfigurator.Configure();
            base.OnStartup(e);
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e) {
            MessageBox.Show("Wystąpił nieoczekiwany błąd: " + e.Exception.Message, "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
            _log.Error(e.Exception);
            e.Handled = true;
        }

    }
}
