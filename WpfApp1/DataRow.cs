﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1 {
    public class DataRow {

        /// <summary>
        /// Długość wiersza danych odbieranych z bezprzewodowych
        /// </summary>
        private static readonly int WRL = 12;

        // czas - bieżacego pomiaru, poprzedniego i ich różnica [s]
        public double ActTime { get; set; }
        public double PrevTime { get; set; }
        public double Delta => ActTime - PrevTime;

        // czy DataRow jest pusty
        private bool _Empty;
        public bool Empty {
            get { return _Empty; }
            private set { _Empty = value; }
        }

        // dane bezpośrednio z czujnika
        public V3 Acc { get; set; }
        public V3 Gyro { get; set; }
        public V3 Mag { get; set; }
        public Quat Q { get; set; }

        // dane przetworzone
        public V3 Euler { get; set; }
        public V3 ActAngles { get; set; }

        // czujnik bezprzewodowy - parametry
        public int Time { get; set; } // milisekundy
        public double Temp { get; set; } // celsjusz
        public double Battery { get; set; } // milivolty

        /// <summary>
        /// Id czujnika, do którego należą dane. Potrzebne do uproszczonej logiki.
        /// </summary>
        public int SensorId { get; set; }

        /// <summary>
        /// Odczytywanie danych z plików
        /// </summary>
        /// <param name="row"></param>
        public DataRow(string row) {
            row = row.Replace(".", ",");
            string[] ar = row.Split(';');

            try {
                ActTime = Math.Round(double.Parse(ar[0]), 2);
                Acc = new V3(ar[1], ar[2], ar[3]);
                Gyro = new V3(ar[4], ar[5], ar[6]);
                Mag = new V3(ar[7], ar[8], ar[9]);
                Euler = new V3(ar[10], ar[11], ar[12]);
                Q = new Quat(ar[13], ar[14], ar[15], ar[16]);
                Q.normalize();

                Empty = false;
            }
            catch (Exception e) {
                 Empty = true;
            }
            finally {
                //this.ToString();
            }
        }

        /// <summary>
        /// To co wyżej + uzupełnianie PrevTime
        /// </summary>
        /// <param name="row"></param>
        /// <param name="time"></param>
        public DataRow(string row, double time) : this(row) {
            PrevTime = time;
        }

        /// <summary>
        /// To co wyżej + uzupełnianie PrevTime
        /// </summary>
        /// <param name="row"></param>
        /// <param name="prevData"></param>
        public DataRow(string row, DataRow prevData) : this(row) {
            PrevTime = prevData.PrevTime;
        }

        /// <summary>
        /// Pusty konstruktor
        /// </summary>
        public DataRow() {
            // Empty = true;
            Acc = new V3();
            Gyro = new V3();
            Mag = new V3();
            Q = new Quat();
            Euler = new V3();
            ActAngles = new V3();
        }

        public DataRow(DataRow dataRow) {
            // no bo skoro dane są kopiowane to pewnie aktualnego odczytu nie ma 
            // więc odczyt jest pusty, tylko zachowujemy ciągłość na wykresie
            Empty = true;

            PrevTime = dataRow.ActTime;
            ActTime = PrevTime + (dataRow.ActTime - dataRow.PrevTime);

            Acc = dataRow.Acc;
            Gyro = dataRow.Gyro;
            Mag = dataRow.Mag;
            Q = dataRow.Q;

            Euler = dataRow.Euler;
            ActAngles = dataRow.ActAngles;

        }

        /// <summary>
        /// Czujniki bezprzewodowe - odbiór danych
        /// </summary>
        /// <param name="array"></param>
        public DataRow(byte[] array, double time, double prevTime) : this() {
            // przypisanie id
            SensorId = array[2];

            // kurwa a gdzie czysczenie inputu z GlobalVariables.RS_DLE ???
            // powinno załatwić sprawę
            List<byte> converted = new List<byte>();

            for(int i = 0; i < array.Length; i++) {
                if(array[i] == GlobalVariables.RS_DLE) {
                    i++;
                    converted.Add((byte)(255 - array[i]));
                }
                else {
                    converted.Add(array[i]);
                }
            }

            int index = 3;
            if((char)converted[index] == 'R') {
                index++;

                //wrzucenie wartości do tablicy - żeby nie kopiować kodu
                short[] tmp = new short[WRL];
                for(int i = 0; i < WRL; i++) {
                    tmp[i] = (short)(converted[index++] | ((short)converted[index++] << 8));
                }
                
                // żyroskop
                double gyroConv = 490 / (Math.Pow(2, 16));
                Gyro = (new V3(tmp[1], tmp[2], tmp[3]) * gyroConv);

                // akcelerometr
                double accConv = 4 * 9.81 / Math.Pow(2, 16);
                Acc = (new V3(tmp[4], tmp[5], tmp[6]) * accConv);

                // magnetometr
                // nie znam przelicznika
                Mag = new V3(tmp[7], tmp[8], tmp[9]);

                // jest jeszcze czas, temperatura i wskaźnik baterii
                Temp = tmp[10] * 0.1;
                Battery = tmp[11];
                Time = (ushort)tmp[0];

            }
            ActTime = time;
            PrevTime = prevTime;
        }

        public override string ToString() {
            return $"{Math.Round(ActTime, 2)};{Acc.ToStr()};{Gyro.ToStr()};{Mag.ToStr()};{Euler.ToStr()};{Q.toStr()}";
        }

        /// <summary>
        /// Metoda kopiująca, używana tylko w przypadku, kiedy trzeba skopiować poprzedni DataRow (bo nie było pomiaru)
        /// </summary>
        /// <returns></returns>
        public DataRow Copy() {
            var dr = new DataRow();
            dr.Acc = new V3(Acc);
            dr.ActAngles = new V3(ActAngles);
            dr.ActTime = ActTime + 0.1; // na sztywno, pomiary i tak są co 100ms
            dr.Battery = Battery;
            dr.Euler = new V3(Euler);
            dr.Gyro = new V3(Gyro);
            dr.Mag = new V3(Mag);
            dr.PrevTime = PrevTime + 0.1;
            dr.Q = new Quat(Q);
            dr.SensorId = SensorId;
            dr.Temp = Temp;
            dr.Time = Time;

            return dr;
        }

    }
}
