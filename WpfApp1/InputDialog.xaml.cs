﻿using System.Windows;

namespace WpfApp1 {
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        private string _notatka;
        private bool _isOk;

        public string Notatka {
            get => _notatka;
            set => _notatka = value;
        }

        public bool IsOk {
            get => _isOk;
            set => _isOk = value;
        }

        public InputDialog()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            Notatka = TextBoxNotatka.Text;
            IsOk = true;
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e) {
            IsOk = false;
            this.Close();
        }
    }
}
