﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.DataAnalysis {

    /// <summary>
    /// Wszystko co dotyczy analizy danych
    /// W wersji ostatecznej trzeba przejrzeć wszystkie komentarze i usunąć niewygodne
    /// Rżnąć na bazie projektu na IWISE - folder na pulpicie i google drive
    /// Tam było ~ 350 linii logiki rozmytej, tu będzie z 1500. Trzeba będzie w książce napisać, skąd wzięły się takie wartości - czyt. naściemniać 
    /// ile się da. Testy (o ile Aneta się zgodzi) powinny pójść bez problemu (przeprost kolana, dziwne kąty zgięcia - komisja powinna łyknąć).
    /// </summary>
    public static class Analysis {

        
        /// <summary>
        /// Inicjalizacja całego silnika FuzzyLogic
        /// </summary>
        public static void Initialise() {

        }

        /// <summary>
        /// Analiza na postawie policzonych danych
        /// Myślę, że kroki można spokojnie pominąć w analizie, chodzi tylko o kąty
        /// </summary>
        /// <param name="data">Dane - tablice kątów</param>
        /// <returns></returns>
        public static AnalysisResult AnalyseData(Data data) {

            throw new NotImplementedException();
        }

    }
}
