﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.DataAnalysis {

    /// <summary>
    /// Typ wyniku - czy dobrze, źle, bardzo źle etc.
    /// </summary>
    public enum Result {

        /// <summary>
        /// Domyślny stane enuma; brak koloru
        /// </summary>
        Default = 0,
        /// <summary>
        /// Wszystko w normie; zielony
        /// </summary>
        Good = 1,
        /// <summary>
        /// Niewielki odbieg od normy (5-10%); żółty
        /// </summary>
        Problem = 2,
        /// <summary>
        /// Źle; pomarańczowy
        /// </summary>
        Bad = 4,
        /// <summary>
        /// Bardzo źle; czerwony
        /// </summary>
        VeryBad = 8,

    }

    /// <summary>
    /// Wynik analizy - na tej podstawie będzie się kolorować gui
    /// Kurwancka, tu będzie masa takiego powtarzalnego kodu - może lepiej na tablicach?
    /// Ew. jakiś słownik?
    /// Tablice nie przejdą bo przydałby się komentarz
    /// </summary>
    public class AnalysisResult {
        // głównie propertasy, metod chyba nie będzie
        // bo po co

        /// <summary>
        /// Symetryczność ruchu - porównanie kątów w obu nogach
        /// </summary>
        public Result Symmetry { get; set; }

        /// <summary>
        /// Rotacja lewej stopy
        /// </summary>
        public Result LeftFootRotation { get; set; }

        /// <summary>
        /// Zgięcie lewej stopy
        /// </summary>
        public Result LeftFootAngle { get; set; }

        /// <summary>
        /// Odwiedzenie lewej stopy
        /// </summary>
        public Result LeftFootAdduction { get; set; }

        public Result LeftKneeRotation { get; set; }

        public Result LeftKneeAngle { get; set; }

        public Result LeftHipRotation { get; set; }

        public Result LeftHipAngle { get; set; }

        public Result LeftHipAdduction { get; set; }

        /// <summary>
        /// Rotacja lewej stopy
        /// </summary>
        public Result RightFootRotation { get; set; }

        /// <summary>
        /// Zgięcie prawej stopy
        /// </summary>
        public Result RightFootAngle { get; set; }

        /// <summary>
        /// Odwiedzenie prawej stopy
        /// </summary>
        public Result RightFootAdduction { get; set; }

        public Result RightKneeRotation { get; set; }

        public Result RightKneeAngle { get; set; }

        public Result RightHipRotation { get; set; }

        public Result RightHipAngle { get; set; }

        public Result RightHipAdduction { get; set; }

        /// <summary>
        /// Obrót (lewo/prawo) miednicy
        /// </summary>
        public Result PelvicRotation { get; set; }

        /// <summary>
        /// Pochylenie przód/tył
        /// </summary>
        public Result PelvicAngle { get; set; }

        /// <summary>
        /// Pochylenie na boki
        /// </summary>
        public Result PelvicAdduction { get; set; }

        
    }
}
